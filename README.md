# Getting Started
- Run the sql script "uob-club-mgmt-db.sql"
- To create a default "ADMIN" user, please update the INSERT STATEMENT in line '214 - 215' at the script
- Update the appication.properties file "Database properties" to your own configuration
- Run the Spring Boot backend project in port 8080
- Run the React JS frontend project in port 3000

# Admin Flow
- Login to default "ADMIN" account created while execute uob-club-mgmt-db.sql
- Click on "profile image icon" navigate to user profile
- Upload a new profile picture and update profile details by click on "save" button once complete edit
- Back To Home, click on "ADMIN" navigate to admin panel
- Navigate to "Event Management" to create a new event

# User(Member) Flow
- Register an account by filling register form or Google OAuth2.0 sign in
- Navigate to "EVENTs" and click on "SEE MORE EVENTS >>"
- By browsing upcoming events, select one of the event
- Click on "here" will navigate to event details
- To register event, click on "Register event" button
- Once registered event, will be able to browse in "Registered Events"

# To Fill-In Attendance and Feedback Form
- Manually type in "http://localhost:3000/attendance-form/{eventId}"
- Manually type in "http://localhost:3000/feedback-form/{eventId}"
- Once completed filling-in form user will earn points
- Users can navigate to "Leaderboard" to view points earned and event registered
- Users can navigate to "User Profile" to check on badge rewarded
- The badges will only reward to users once they earned specific amount of points 

# Task Allocation
User (Jayson)
 - Role Based Access Control (superadmin, admin, members, users)
 - Google Oauth Signin
 - Forgot Password (Reset Password)
 - Update User Profile and Details
 - Register (Activation) and Login
 - Remove account (admin and members)

Badges and Point - Leaderboard (Jayson)
 - summarize users' point into Leaderboard
 - five badges (A, B, C, D, E)
 - display badges at user profile
 - view history point (All-time and monthly)

Event (Haikhqal)
 - create event (admin)
 - generate QR (only admin and super admin)
 - view registered event, joined event, upcoming event
 - edit event details (admin)
 - remove event (admin)

Registration Event (Haikhqal)
 - register event
 - get email notification (register successful)
 - view event registration (admin)

Attendance (Batrisyia)
 - notifications email (reminder)
 - attendace form
 - view event attendance (admin)
 - view attendace status (display in registered event)
 - notification email (attendance taken)

Feedback (Batrisyia)
 - notifications email (reminder)
 - feedback form 
 - view event feedback (admin)
 - summarize feedback result (chart)
 - view feedback status (display in registered event)
 - notification email (feedback form complete)
