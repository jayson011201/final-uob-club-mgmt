import React, { useEffect } from 'react';
import './App.css';
import { BrowserRouter, Route, Routes, useLocation, Navigate, useNavigate } from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';

import FeedbackSummary from './Component/AdminComponent/FeedbackManagements/FeedbackSummary';
import Home from './Component/Home';
import Login from './Component/UserComponent/Login';
import Menu from './Component/Menu';
import ContactUs from "./Component/ContactUs";
import Register from "./Component/UserComponent/Register";
import ForgotPassword from "./Component/UserComponent/ForgotPassword";
import ResetPassword from "./Component/UserComponent/ResetPassword";
import AdminMenu from "./Component/AdminComponent/AdminMenu";
import UserManagement from "./Component/AdminComponent/UserManagement";
import BadgeManagement from "./Component/AdminComponent/BadgeManagement";
import PointManagement from "./Component/AdminComponent/PointManagement";
import FeedbackManagement from "./Component/AdminComponent/FeedbackManagement";
import AttendanceManagement from "./Component/AdminComponent/AttendanceManagement";
import EventManagement from "./Component/AdminComponent/EventManagement";
import UserProfile from "./Component/UserComponent/UserProfile";
import Leaderboard from "./Component/UserComponent/Leaderboard";
import GoogleSignIn from "./Component/GoogleSignIn";
import NotFound from "./Component/UserComponent/NotFound";
import AttendanceForm from "./Component/UserComponent/AttendanceForm";
import CreateEvent from './Component/AdminComponent/EventComponent/CreateEvent';
import ViewFeedback from "./Component/AdminComponent/FeedbackManagements/ViewFeedback"
import ViewAttendance from './Component/AdminComponent/AttendanceManagements/ViewAttendance';
import PostEventInfo from './Component/AdminComponent/Attendance';
import FeedbackForm from './Component/UserComponent/FeedbackForm';
import EditEvent from './Component/AdminComponent/EventComponent/EditEvent';
import DetailsEvent from './Component/AdminComponent/EventComponent/DetailsEvent';
import ListEvent from "./Component/UserComponent/ListEvent";
import RegisterEvent from "./Component/RegisterComponent/RegisterEvent";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <AppRouter />
                <CssBaseline />
            </BrowserRouter>
        </div>
    );
}

function AppRouter() {
    const location = useLocation();
    const navigate = useNavigate();

    const publicPaths = ['/home', '/login', '/register', '/forgot-password', '/google-sign-in', '/page-not-found', '/reset-password'];
    const memberPaths = ['/member'];
    const adminPaths = ['/admin'];

    useEffect(() => {
        const userRole = localStorage.getItem('role');
        const isPublicPath = publicPaths.includes(location.pathname);

        if ((!userRole && location.pathname.startsWith('/attendance-form')) || (!userRole && location.pathname.startsWith('/feedback-form'))) {
            //navigate('/login');
            // return;
            const eventId = extractEventIdFromPath(location.pathname);
            const type = extractTypeFromPath(location.pathname);
            navigate(`/login?eventId=${eventId}&type=${type}`);
            return;
        }else if (!userRole && !isPublicPath){
            navigate("/home")
        }

        // Restrict access for USER role
        if (userRole === 'USER' && !location.pathname.startsWith('/member')) {
            navigate('/member/home');
        }

        // Restrict access for ADMIN role
        if (userRole === 'ADMIN' && !location.pathname.startsWith('/admin')) {
            navigate('/admin/home');
        }
    }, [location, navigate]);

    const extractEventIdFromPath = (path) => {
        const parts = path.split('/');
        return parts[2]; // Assuming eventId is the third part in the path
    };

    const extractTypeFromPath = (path) => {
        const parts = path.split('/');
        return parts[1]; // Assuming eventId is the third part in the path
    };


    const isContactUsVisible = ['/home', '/admin/home', '/member/home'].includes(location.pathname);

    return (
        <>
            <Routes>
                {/* Place more specific routes before wildcard routes */}
                <Route path='/home' element={<HomeRoutes />} />
                <Route path='/login' element={<Login />} />
                <Route path='/google-sign-in' element={<GoogleSignIn />} />
                <Route path='/register' element={<Register />} />
                <Route path='/reset-password' element={<ResetPassword />} />
                <Route path='/forgot-password' element={<ForgotPassword />} />
                {/* Add routes for other public pages */}
                <Route path='/member/*' element={<MemberRoutes />} />
                <Route path='/admin/*' element={<AdminRoutes />} />
                {/* Allow unauthenticated users to access AttendanceForm */}
                <Route path='/attendance-form/*' element={<AttendanceForm />} />
                <Route path='/feedback-form/*' element={<FeedbackForm />} />
            </Routes>
        </>
    );
}

function HomeRoutes() {
    return (
        <>
            <Routes>
                <Route path='/' element={<Home />} />
            </Routes>
        </>
    );
}

function MemberRoutes() {
    const location = useLocation();

    return (
        <>
            <Routes>
                <Route path='home' element={<Home />} />
                <Route path='attendance-form/:eventId' element={<AttendanceForm />} />
                <Route path='feedback-form/:eventId' element={<FeedbackForm />} />
                <Route path='profile' element={<UserProfile />} />
                <Route path='leaderboard' element={<Leaderboard />} />
                <Route path='/list-event/:userId' element={<ListEvent />} />
                <Route path='*' element={<NotFound />} />
                <Route path='/register/:id' element={<RegisterEvent />} />
                {/* Add routes for other member pages */}
            </Routes>
        </>
    );
}




function AdminRoutes() {
    const location = useLocation();
    const userRole = localStorage.getItem('role');

    return (
                    <Routes>
                        {/* Add other admin routes */}
                        <Route path='user' element={<UserManagement />} />
                        <Route path='event' element={<EventManagement />} />
                        <Route path='attendance' element={<AttendanceManagement />} />
                        <Route path='feedback' element={<FeedbackManagement />} />
                        <Route path='point' element={<PointManagement />} />
                        <Route path='badge' element={<BadgeManagement />} />
                        <Route path='profile' element={<UserProfile />} />
                        <Route path='leaderboard' element={<Leaderboard />} />
                        <Route path='create-event' element={<CreateEvent />} />
                        <Route path='view-attendance/:eventId' element={<ViewAttendance />} />
                        <Route path='view-feedback/:eventId' element={<ViewFeedback />} />
                        <Route path='attendance-form/:eventId' element={<AttendanceForm />} />
                        <Route path='feedback-form/:eventId' element={<FeedbackForm />} />
                        <Route path='register/:id' element={<RegisterEvent />} />
                        <Route path='/event-details/:id' element={<DetailsEvent />} />
                        <Route path='/edit-event/:eventId' element={<EditEvent />} />
                        <Route path='/list-event/:userId' element={<ListEvent />} />
                        {/* Add a route for the admin home page */}
                        <Route path='home' element={<AdminHome />} />
                        <Route path='*' element={<NotFound />} />
                    </Routes>
    );
}




function AdminHome() {
    return (
            <Routes>
                <Route path='/' element={<Home />} />
            </Routes>
    );
}

export default App;