import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { GoogleLogin, googleLogout, useGoogleLogin } from '@react-oauth/google';

function GoogleSignIn(props) {
    const [userInfo, setUserInfo] = useState([]);
    const [profileInfo, setProfileInfo] = useState([]);
    const responseOutput = (response) => {
        console.log(response);
    };
    const errorOutput = (error) => {
        console.log(error);
    };

    useEffect(
        () => {
            if (userInfo) {
                axios
                    .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${userInfo.access_token}`, {
                        headers: {
                            Authorization: `Bearer ${userInfo.access_token}`,
                            Accept: 'application/json'
                        }
                    })
                    .then((response) => {
                        setProfileInfo(response.data);
                        console.log("Profile Info:", response.data); // Log the profile info
                    })
                    .catch((error) => console.log(error));
            }
        },
        [userInfo]
    );

    const logOut = () => {
        googleLogout();
        setProfileInfo(null);
    };

    const login = useGoogleLogin({
        onSuccess: (response) => {
            setUserInfo(response);
            console.log("User Info:", response); // Log the user info
        },
        onError: (error) => console.log(`Login Failed: ${error}`)
    });

    return (
        <div>
            <h2>React Google Login</h2>
            {profileInfo ? (
                <div>
                    <img src={profileInfo.picture} alt="Profile Image" />
                    <h3>Currently logged in user</h3>
                    <p>Name: {profileInfo.name}</p>
                    <p>Email: {profileInfo.email}</p>
                    <br />
                    <br />
                    <button onClick={logOut}>Log out</button>
                </div>
            ) : (
                <button onClick={() => login()}>Sign in</button>
            )}
            <br />
            <br />
        </div>
    );
}

export default GoogleSignIn;
