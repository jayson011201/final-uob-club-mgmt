import React, { useState, useEffect } from 'react';
import {Grid, Paper, Typography, TextField, Button, Link, Alert, Box, IconButton, Modal} from '@mui/material';
import logo from '../../Asset/logo.png';
import { Facebook, Google } from '@mui/icons-material';
import {Link as RouterLink, useLocation, useNavigate} from 'react-router-dom';
import axios from 'axios';
import Snackbar from '@mui/material/Snackbar';
import {googleLogout, useGoogleLogin} from "@react-oauth/google";
import CloseIcon from "@mui/icons-material/Close";

function Login() {
    const [email, setEmail] = useState('');
    const [googleEmail, setGoogleEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [isSuccessAlertOpen, setIsSuccessAlertOpen] = useState(false);
    const [isErrorAlertOpen, setIsErrorAlertOpen] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [activationCode, setActivationCode] = useState('');
    const navigate = useNavigate();
    const location = useLocation();

    const [open, setOpen] = useState(false);

    const [openModal, setOpenModal] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const isOpenPasswordModal = () => setOpenModal(true);
    const isClosePasswordModal = () => setOpenModal(false);

    const [userInfo, setUserInfo] = useState([]);
    const [profileInfo, setProfileInfo] = useState([]);

    const responseOutput = (response) => {
        console.log(response);
    };
    const errorOutput = (error) => {
        console.log(error);
    };

    const logOut = () => {
        googleLogout();
        setProfileInfo(null);
    };

    const login = useGoogleLogin({
        onSuccess: (response) => {
            setUserInfo(response);
            console.log("User Info:", response); // Log the user info
            isOpenPasswordModal();
        },
        onError: (error) => console.log(`Login Failed: ${error}`)
    });

    useEffect(
        () => {
            if (userInfo) {
                axios
                    .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${userInfo.access_token}`, {
                        headers: {
                            Authorization: `Bearer ${userInfo.access_token}`,
                            Accept: 'application/json'
                        }
                    })
                    .then((response) => {
                        setProfileInfo(response.data);
                        setEmail(response.data.email);
                        console.log("Profile Info:", response.data); // Log the profile info
                    })
                    .catch((error) => console.log(error));
            }
        },
        [userInfo]
    );

    const handleLogin = async (event) => {
        event.preventDefault();

        try {
            const response = await axios.post('http://localhost:8080/api/v1/auth/signin', {
                email: email,
                password: password,
            });

            const queryParams = new URLSearchParams(location.search);
            const eventId = queryParams.get('eventId');
            const type = queryParams.get('type');

            if (response.status === 200 && response.data.token) {
                console.log(response.data);
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('email', response.data.user.email);
                localStorage.setItem('id', response.data.user.id);
                localStorage.setItem('role', response.data.user.role);
                localStorage.setItem('fullname', response.data.user.fullname);
                console.log(response.data);
                if(response.data.user.role === "USER"){
                    if (eventId) {
                        if (type === "attendance-form" ){
                            setIsSuccessAlertOpen(true);
                            setTimeout(() => {
                                navigate(`/member/attendance-form/${eventId}`);
                            }, 2000);
                        }

                        if (type === "feedback-form" ){
                            setIsSuccessAlertOpen(true);
                            setTimeout(() => {
                                navigate(`/member/feedback-form/${eventId}`);
                            }, 2000);
                        }

                    } else {
                        setIsSuccessAlertOpen(true);
                        setTimeout(() => {
                            navigate(`/member/home`);
                        }, 2000);
                    }
                }
                if(response.data.user.role === "ADMIN"){
                    if (eventId) {
                        setIsSuccessAlertOpen(true);
                        setTimeout(() => {
                            navigate(`/admin/attendance-form/${eventId}`);
                        }, 2000);
                    } else {
                        setIsSuccessAlertOpen(true);
                        setTimeout(() => {
                            navigate(`/admin/home`);
                        }, 2000);
                    }
                }
            } else {
                throw new Error('Login Failed');
            }
            console.log(response.data);
        } catch (error) {
            setErrorMessage('Invalid credentials. Please try again...');
            setIsErrorAlertOpen(true);
            console.error('Login failed', error.response || error);
        }
    };

    const handleGoogleSignIn = async () => {
        try {
            await login();
        } catch (error) {
            console.error('Error initiating Google login:', error);
        }
    };

    const handleRegistration = async () => {
        try {
            const registraterData = {
                email,
                password,
            };

            console.log("handle registration")
            console.log(registraterData);

            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/google-sign-up',
                registraterData
            );

            console.log('Registration successful', response.data);

            setOpenModal(false);
            handleOpen();
        } catch (error) {
            console.error('Registration error', error);
        }
    };

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
        isClosePasswordModal();
    };

    const handleActivation = async () => {
        try {
            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/verify-otp',
                { code: activationCode }
            );

            if (response.status === 200) {
                console.log('Activation Successful');
            } else {
                console.error('Invalid activation code');
            }

            // Close the modal
            handleClose();
        } catch (error) {
            console.error('Activation error', error);
        }
    };

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    };

    return (
        <Grid container component="main" sx={{ height: '100vh', backgroundColor: (theme) => theme.palette.primary.main }}>
            {/* Left section with gradient background and logo */}
            <Grid item xs={false} sm={4} md={7} sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                borderTopRightRadius: '200px',
                borderBottomRightRadius: '200px',
            }}>
                <RouterLink to={"/"}>
                    <img src={logo} alt="Logo" style={{ maxWidth: '80%', height: 'auto', marginLeft: '10%' }} />
                </RouterLink>
            </Grid>

            {/* Right section for the login form with blue background */}
            <Grid item xs={12} sm={8} md={5} sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: (theme) => theme.palette.primary.main,
                height: '100vh',
                textAlign: 'center',
            }}>
                <Paper elevation={6} square sx={{
                    my: 0,
                    mx: 4,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 'auto',
                    width: '100%',
                    maxWidth: '400px',
                    p: 3,
                }}>
                    {/* Login form */}
                    <form onSubmit={handleLogin}>
                        <Typography component="h2" variant="h5" sx={{ fontWeight: 'bold' }}>
                            Login
                        </Typography>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Username or email"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            value={email}
                            onChange={handleEmailChange}
                        />
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            value={password}
                            onChange={handlePasswordChange}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Login
                        </Button>
                        <Typography component="p" variant="body2" sx={{ mt: 2 }}>
                            or
                        </Typography>
                        <Grid container justifyContent="center" sx={{ mt: 2 }}>
                            <Grid item>
                                <Button
                                    variant="outlined"
                                    startIcon={<Google />}
                                    onClick={() => handleGoogleSignIn()}
                                >
                                    Google
                                </Button>
                            </Grid>
                        </Grid>
                        <Typography component="p" variant="body2" sx={{ mt: 2 }}>
                            <Link href="/forgot-password" variant="body2">
                                {"Forgot Password?"}
                            </Link>
                            {"\u00A0|\u00A0"}
                            <Link href="/register" variant="body2">
                                {"Sign Up"}
                            </Link>
                        </Typography>
                    </form>
                </Paper>
            </Grid>
            <Snackbar
                open={isSuccessAlertOpen}
                autoHideDuration={2000}
                onClose={() => setIsSuccessAlertOpen(false)}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                elevation={3}
            >
                <Alert onClose={() => setIsSuccessAlertOpen(false)} severity="success">
                    Login successful! Redirecting...
                </Alert>
            </Snackbar>

            <Snackbar
                open={isErrorAlertOpen}
                autoHideDuration={4000}
                onClose={() => setIsErrorAlertOpen(false)}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                elevation={3}
            >
                <Alert onClose={() => setIsErrorAlertOpen(false)} severity="error">
                    {errorMessage}
                </Alert>
            </Snackbar>

            <Modal
                open={openModal}
                aria-labelledby="activation-code-modal"
                aria-describedby="activation-code-input"
            >
                <Box sx={modalStyle}>
                    <IconButton
                        onClick={isClosePasswordModal}
                        aria-label="close"
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography id="activation-code-modal" variant="h6" component="h2">
                        Enter you account password
                    </Typography>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="new-password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="confirmPassword"
                        label="Confirm Password"
                        type="password"
                        id="confirmPassword"
                        autoComplete="new-password"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleRegistration}
                    >
                        Enter
                    </Button>
                </Box>
            </Modal>

            <Modal
                open={open}
                aria-labelledby="activation-code-modal"
                aria-describedby="activation-code-input"
            >
                <Box sx={modalStyle}>
                    <IconButton
                        onClick={handleClose}
                        aria-label="close"
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography id="activation-code-modal" variant="h6" component="h2">
                        Check email for activation code
                    </Typography>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="activation-code"
                        label="Activation Code"
                        name="activation"
                        autoComplete="off"
                        autoFocus
                        value={activationCode}
                        onChange={(e) => setActivationCode(e.target.value)}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleActivation}
                        //href={'/login'}
                    >
                        Enter
                    </Button>
                </Box>
            </Modal>
        </Grid>
    );
}

export default Login;
