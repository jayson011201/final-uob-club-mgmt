import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
    Container, Tabs, Tab, Paper, Table,
    TableBody, TableCell, TableContainer, TableHead, TableRow
} from '@mui/material';
import Menu from "../Menu";

function Leaderboard() {
    const [leaderboardData, setLeaderboardData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [selectedTab, setSelectedTab] = useState(0); // 0 for event, 1 for points
    const [selectedMonth, setSelectedMonth] = useState('All Time'); // Initial selection

    const defaultProfileImage = "default.jfif";

    useEffect(() => {
        axios.get('http://localhost:8080/api/v1/auth/users-points-attendance')
            .then(response => {
                const processedData = processData(response.data);
                setLeaderboardData(processedData);
            })
            .catch(error => {
                console.error('Error fetching data: ', error);
            });
    }, []);

    useEffect(() => {
        const newFilteredData = filterData(leaderboardData, selectedTab, selectedMonth);
        setFilteredData(newFilteredData);
    }, [leaderboardData, selectedTab, selectedMonth]);

    const handleTabChange = (event, newValue) => {
        setSelectedTab(newValue);
    };

    const handleMonthChange = (event, newValue) => {
        setSelectedMonth(newValue);
    };

    function processData(data) {
        return data.map(user => {
            const totalPoints = user.points.reduce((acc, curr) => acc + Number(curr.pointAmount), 0);
            const eventsJoined = user.attendances.length;

            return {
                id: user.id,
                email: user.email, // Assuming email as username
                points: totalPoints,
                eventsJoined: eventsJoined,
                profilePicture: user.profile, // Adjust the path as needed
                pointsDetails: user.points,
                attendancesDetails: user.attendances
            };
        });
    }

    function filterData(data, selectedTab, selectedMonth) {
        let sortedAndFilteredData = data.map(user => {
            if (selectedTab === 0) { // Events
                const eventsInMonth = user.attendancesDetails.filter(attendance => isMonth(attendance.attendanceDate, selectedMonth));
                return { ...user, eventsJoined: eventsInMonth.length };
            } else { // Points
                const pointsInMonth = user.pointsDetails.filter(point => isMonth(point.pointEarnDate, selectedMonth));
                const totalPointsInMonth = pointsInMonth.reduce((acc, curr) => acc + Number(curr.pointAmount), 0);
                return { ...user, points: totalPointsInMonth };
            }
        });

        // Sort by points or events in descending order
        sortedAndFilteredData.sort((a, b) => {
            if (selectedTab === 0) { // Events
                return b.eventsJoined - a.eventsJoined;
            } else { // Points
                return b.points - a.points;
            }
        });

        // Adding rank
        sortedAndFilteredData.forEach((user, index) => {
            user.rank = index + 1;
        });
        return sortedAndFilteredData;
    }

    function isMonth(dateStr, month) {
        if (month === 'All Time') return true;
        const date = new Date(dateStr);
        return date.toLocaleString('default', { month: 'long' }) === month;
    }

    const getFilenameFromPath = (filePath) => {
        if (!filePath) {
            return "default.jfif"; // or handle the default case as appropriate
        }

        const parts = filePath.split(/\\+/);
        return parts[parts.length - 1];
    };



    return (
        <Container sx={{ marginTop: '120px' }}>
            <Menu/>
            <Tabs
                value={selectedTab}
                onChange={handleTabChange}
                indicatorColor="primary"
                textColor="primary"
                centered
            >
                <Tab label="Events" />
                <Tab label="Points" />
            </Tabs>

            <Tabs
                value={selectedMonth}
                onChange={handleMonthChange}
                indicatorColor="primary"
                textColor="primary"
                variant="scrollable"
                scrollButtons="auto"
            >
                <Tab label="All Time" value="All Time" />
                <Tab label="Jan" value="January" />
                <Tab label="Feb" value="Febuary" />
                <Tab label="Mar" value="March" />
                <Tab label="Apr" value="April" />
                <Tab label="May" value="May" />
                <Tab label="Jun" value="June" />
                <Tab label="Jul" value="July" />
                <Tab label="Aug" value="August" />
                <Tab label="Sept" value="September" />
                <Tab label="Oct" value="October" />
                <Tab label="Nov" value="November" />
                <Tab label="Dec" value="December" />
            </Tabs>

            <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Rank</TableCell>
                            <TableCell>Profile Picture</TableCell>
                            <TableCell>Email</TableCell>
                            {selectedTab === 0 ? <TableCell>Total Events Joined</TableCell> : <TableCell>Total Points</TableCell>}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {filteredData.map((user) => {
                            const filename = getFilenameFromPath(user.profilePicture);

                            return (
                                <TableRow key={user.id}>
                                    <TableCell>{user.rank}</TableCell>
                                    <TableCell>
                                        <img src={`http://localhost:8080/api/v1/auth/images/${filename}`} alt="Profile" style={{ width: '50px', height: '50px' }}
                                        />
                                    </TableCell>
                                    <TableCell>{user.email}</TableCell>
                                    {selectedTab === 0 ? <TableCell>{user.eventsJoined}</TableCell> : <TableCell>{user.points}</TableCell>}
                                </TableRow>
                            );
                        })}
                    </TableBody>


                </Table>
            </TableContainer>
        </Container>
    );
};

export default Leaderboard;
