import React, { useState } from 'react';
import {
    Box,
    Modal,
    Grid,
    Paper,
    Typography,
    TextField,
    Button,
    Link,
    IconButton,
    Alert,
    LinearProgress
} from '@mui/material';
import logo from '../../Asset/logo.png';
import {Link as RouterLink} from 'react-router-dom';
import Snackbar from "@mui/material/Snackbar";
import Axios from 'axios';

function ForgotPassword() {
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [isErrorAlertOpen, setIsErrorAlertOpen] = useState(false);
    // const [isLoadingShow, setIsLoadingShow] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const handleButtonClick = () => {
        const email = document.getElementById('email').value;

        Axios.post('http://localhost:8080/api/v1/auth/reset-password', { email })
            .then((response) => {
                // setIsLoadingShow(true);
                if (response.status === 200) {
                    // setIsLoadingShow(false);
                    setOpenSnackbar(true);
                }
            })
            .catch((error) => {
                setErrorMessage('Email not found. Please enter a registered email...');
                setIsErrorAlertOpen(true);
                console.error('Email not registered:', error);
            });
    };

    const handleCloseSnackbar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnackbar(false);
    };

    return (
        <Grid container component="main" sx={{ height: '100vh', backgroundColor: (theme) => theme.palette.primary.main, }}>
            {/* Left section with gradient background and logo */}
            <Grid item xs={false} sm={4} md={7} sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                borderTopRightRadius: '200px',
                borderBottomRightRadius: '200px',
            }}>
                <RouterLink to={"/"}>
                    <img src={logo} alt="Logo" style={{ maxWidth: '80%', height: 'auto' }} />
                </RouterLink>
            </Grid>

            {/* Right section for the login form with blue background */}
            <Grid item xs={12} sm={8} md={5} sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: (theme) => theme.palette.primary.main, // This sets the background color of the right section to blue
                height: '100vh', // This ensures it covers the full height of the viewport
            }}>
                <Paper elevation={6} square sx={{
                    my: 0, // Use the full height available
                    mx: 4, // Horizontal margin
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: 'auto', // Height is set to auto to wrap the content
                    width: '100%', // Use the full width available
                    maxWidth: '400px', // Maximum width for aesthetics
                    p: 3, // Padding inside the Paper component
                }}>
                    {/* Login form */}
                    <Typography component="h2" variant="h5" sx={{ fontWeight: 'bold' }}>
                        Forgot Password
                    </Typography>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Registered email"
                        name="email"
                        autoComplete="email"
                        autoFocus
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleButtonClick}
                    >
                        Enter
                    </Button>
                    <Typography component="p" variant="body2" sx={{ mt: 2 }}>
                        <Link href="/login" variant="body2">
                            {"Login"}
                        </Link>
                        {"\u00A0|\u00A0"}
                        <Link href="/register" variant="body2">
                            {"Sign Up"}
                        </Link>
                    </Typography>
                </Paper>
            </Grid>
            <Snackbar
                open={openSnackbar}
                autoHideDuration={4000}
                onClose={() => handleCloseSnackbar()}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                elevation={3}
            >
                <Alert onClose={() => handleCloseSnackbar()} severity="success">
                    Check your email for reset password link.
                </Alert>
            </Snackbar>
            <Snackbar
                open={isErrorAlertOpen}
                autoHideDuration={4000}
                onClose={() => setIsErrorAlertOpen(false)}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                elevation={3}
            >
                <Alert onClose={() => setIsErrorAlertOpen(false)} severity="error">
                    {errorMessage}
                </Alert>
            </Snackbar>
        </Grid>
    );
}

export default ForgotPassword;
