import { useState, useEffect } from 'react';
import { Box, Grid, Paper, Typography, TextField, Button, Link } from '@mui/material';
import logo from '../../Asset/logo.png';
import { Link as RouterLink } from 'react-router-dom';
import axios from 'axios';

function ResetPassword() {
    const [tokenValid, setTokenValid] = useState(null);
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');

    useEffect(() => {
        const queryParams = new URLSearchParams(window.location.search);
        const token = queryParams.get('token');

        if (!token || token.trim() === '') {
            window.location.href = 'http://localhost:3000/page-not-found';
            return;
        }

        axios
            .get(`http://localhost:8080/api/v1/auth/verify-link?token=${token}`)
            .then((response) => {
                if (response.status === 200) {
                    setTokenValid(true);
                } else {
                    setTokenValid(false);
                }
            })
            .catch((error) => {
                console.error(error);
                // Redirect to an error page or display an error message
                setTokenValid(false);
            });
    }, []);

    const handlePasswordReset = () => {
        // Check if passwords match
        if (password !== confirmPassword) {
            alert("Passwords do not match. Please try again.");
            return;
        }

        const queryParams = new URLSearchParams(window.location.search);
        const token = queryParams.get('token');

        axios.post(`http://localhost:8080/api/v1/auth/submit-reset-password`, {
            token,
            newPassword: password, // assuming newPassword is the key expected by your backend
        })

            .then((response) => {
                if (response.status === 200) {
                    alert("Password reset successfully. Redirecting to login.");
                    window.location.href = '/login';
                } else {
                    alert("Password reset failed. Please try again. Response: " + JSON.stringify(response.data));
                }
            })
            .catch((error) => {
                console.error(error);
                alert("An error occurred. Please try again. Error: " + error.message);
            });

    };

    return (
        <Grid
            container
            component="main"
            sx={{ height: '100vh', backgroundColor: (theme) => theme.palette.primary.main }}
        >
            {/* Left section with gradient background and logo */}
            <Grid
                item
                xs={false}
                sm={4}
                md={7}
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'white',
                    borderTopRightRadius: '200px',
                    borderBottomRightRadius: '200px',
                }}
            >
                <RouterLink to={'/'}>
                    <img src={logo} alt="Logo" style={{ maxWidth: '80%', height: 'auto' }} />
                </RouterLink>
            </Grid>

            {/* Right section for the login form with blue background */}
            <Grid
                item
                xs={12}
                sm={8}
                md={5}
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: (theme) => theme.palette.primary.main,
                    height: '100vh',
                }}
            >
                <Paper
                    elevation={6}
                    square
                    sx={{
                        my: 0,
                        mx: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 'auto',
                        width: '100%',
                        maxWidth: '400px',
                        p: 3,
                    }}
                >
                    {tokenValid === null ? (
                        <Typography component="div">Loading...</Typography>
                    ) : tokenValid === false ? (
                        <Typography component="div">Invalid token. Please try again.</Typography>
                    ) : (
                        // Token is valid, allow the user to reset the password
                        <>
                            <Typography component="h2" variant="h5" sx={{ fontWeight: 'bold' }}>
                                Reset Password
                            </Typography>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="New Password"
                                type="password"
                                id="password"
                                autoComplete="new-password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                name="confirmPassword"
                                label="Confirm Password"
                                type="password"
                                id="confirmPassword"
                                autoComplete="new-password"
                                value={confirmPassword}
                                onChange={(e) => setConfirmPassword(e.target.value)}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                                onClick={handlePasswordReset}
                            >
                                Reset Password
                            </Button>
                            <Typography component="p" variant="body2" sx={{ mt: 2 }}>
                                <Link component={RouterLink} to="/login" variant="body2">
                                    Login
                                </Link>
                                {"\u00A0|\u00A0"}
                                <Link component={RouterLink} to="/register" variant="body2">
                                    Sign Up
                                </Link>
                            </Typography>
                        </>
                    )}
                </Paper>
            </Grid>
        </Grid>
    );
}

export default ResetPassword;
