import {useEffect, useRef, useState} from "react";
import emailjs from '@emailjs/browser';
import {useParams} from "react-router-dom";
import {Button, Paper, TextField} from "@mui/material";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';

function AttendanceForm() {
    let userId = localStorage.getItem("id");
    console.log(userId);

    let localemail = localStorage.getItem("email");
    let {eventId} = useParams();
    // console.log(eventId);
    const eventDate = "2023/11/16";
    const currentDate = new Date();
    console.log(currentDate);
    // success message
    const [successMessage, setSuccessMessage] = useState("");

    // declare hook to fetch value from the form

    const [StatusValue, setStatusValue] = useState("");
    const [nameValue, setNameValue] = useState("");
    const [emailValue, setEmailValue] = useState(localemail || "");

    // update state when input values change
    const handleNameChange = (event) => {
        setNameValue(event.target.value);
    };

    const handleEmailChange = (event) => {
        setEmailValue(event.target.value);
    };

    const handleStatusChange = (event) => {
        setStatusValue(event.target.value);
    };

    // hook to populate existing data from the db
    const [userData, setUserData] = useState({});

    // fetch data from db based on id
    const fetchData = () => {
        fetch(`http://localhost:8080/api/v1/auth/users/${userId}`)
            .then((response) => response.json())
            .then((json) => {
                console.log(json);
                setUserData(json);
            })
            .catch((error) => {
                console.log(error);
            });
    };

    // email notification success attendance using emailjs
    const sendEmail = (event) => {
        var templateParams = {
            to: emailValue,
            to_name: nameValue,
            title: 'Attendance' //set title
        };
        event.preventDefault();

        emailjs
            .send(
                'service_tukj9jp',
                'template_k50hm6t',
                templateParams,
                'gkBuG0ArjFjG5xwbl'
            )
            .then(
                function (response) {
                    console.log('SUCCESS!', response.status, response.text);
                },
                function (error) {
                    console.log('FAILED...', error);
                }
            );
    };

    // reload
    useEffect(() => {
        fetchData();
    }, []);

    // submit value to the attendance table
    function submitAttendance(event) {
        event.preventDefault(); // Prevents the default form submission behavior


        const data = {name: nameValue, email: emailValue, status: StatusValue, userId, eventId};
        console.log(data);
        let url = 'http://localhost:8080/attendance-form'; // database URL

        let setting = {
            method: 'put',
            body: JSON.stringify(data),
            headers: {'Content-type': 'application/json'}
        };
        fetch(url, setting)
            .then((response) => {
                if (response.ok) {

                    const contentType = response.headers.get('content-type');
                    if (contentType && contentType.includes('application/json')) {
                        return response.json();

                    } else {
                        // If the response is not JSON, treat it as plain text
                        return response.text();
                    }
                } else {
                    throw new Error("Failed to submit ");
                }


            })
            .then((data) => {
                console.log('Response from server:', data);
                sendEmail(event);

            })
            .catch((error) => {
                // Handle the error
                console.error('Error submitting:', error);
                // You may want to handle the error more gracefully, display a message, etc.
            });
    }

    return (
        <div className="">
            <div className="max-w-fit px-5 mx-auto mt-44">

                <Paper className="px-5 rounded-xl " elevation={2}>
                    <div className="">
                        <FormControl>
                            <div className="mt-4 text-left">
                                <FormLabel id="demo-row-radio-buttons-group-label">Confirm your details</FormLabel>
                            </div>
                            <div>
                                <TextField id="filled-basic" label="Name" variant="filled" value={nameValue}
                                           onChange={handleNameChange}/>
                            </div>
                            <div className="my-3">
                                <TextField id="filled-basic" label="Email" variant="filled" value={emailValue}
                                           onChange={handleEmailChange}/>
                            </div>
                            <div className="my-4">
                                <Button onClick={(event) => submitAttendance(event)} variant="contained">Submit
                                    Attendance</Button>
                            </div>
                        </FormControl>
                    </div>
                </Paper>
            </div>
        </div>
    );
}

export default AttendanceForm;
