import React, {useState} from 'react'
import emailjs from '@emailjs/browser';
import {useParams} from 'react-router-dom';
import {Button} from "@mui/material";


const FeedbackForm = () => {

    //properties
    const [showThankYou, setShowThankYou] = useState(false);
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [answer, setAnswers] = useState([]);
    const [question1, setQuestion1] = useState('');
    const [question2, setQuestion2] = useState('');
    const [question3, setQuestion3] = useState('');
    const [question4, setQuestion4] = useState('');

    const email = localStorage.getItem("email");
    const name = "Sarah";
    const userId = localStorage.getItem("id");
    console.log(userId);
    console.log(email);


    let {eventId} = useParams();
    // console.log(eventId);
    const eventDate = "2023/11/16";
    const currentDate = new Date();


    //insert answer in database
    function saveFeedback() {
        const data =
            {
                question1,
                question2,
                question3,
                question4,
                answer1: answer[0],
                answer2: answer[1],
                answer3: answer[2],
                answer4: answer[3],
                userId,
                eventId,
                status: "Yes",
                name,
                email
            };

        let url = 'http://localhost:8080/feedback-form'; //database url

        let setting = {
            method: 'put',
            body: JSON.stringify(data),
            headers: {'Content-type': 'application/json'}
        };

        fetch(url, setting)
            .then((response) => {
                if (response.ok) {
                    sendEmail();
                    const contentType = response.headers.get('content-type');
                    if (contentType && contentType.includes('application/json')) {
                        return response.json();
                    } else {
                        // If the response is not JSON, treat it as plain text
                        return response.text();
                    }
                } else {
                    throw new Error("Failed to submit feedback");
                }
            })
            .then((data) => {
                console.log('Response from server:', data);
                // saveFeedback();
                // Additional logic if needed based on the successful response
                // For example, you can call sendEmail() here
            })
            .catch((error) => {
                // Handle the error
                console.error('Error submitting feedback:', error);
                // You may want to handle the error more gracefully, display a message, etc.
            });
    }

    //email notification for feedback
    const sendEmail = () => {
        var templateParams = {
            to: email, //set receiver email
            to_name: name, //set receiver name
            title: 'Feedback' //set title
        };

        emailjs
            .send(
                'service_tukj9jp',
                'template_k50hm6t',
                templateParams,
                'gkBuG0ArjFjG5xwbl'
            )
            .then(
                function (response) {
                    console.log('SUCCESS!', response.status, response.text);
                },
                function (error) {
                    console.log('FAILED...', error);
                }
            );
    };

    //question
    const question = [
        "What do you think about the event?",
        "What is your thought on the food served?",
        "What do you think about the event's activities?",
        "What is your thought on the event's management?"
    ]

    //if list is selected
    const optionClicked = (option) => {
        if (currentQuestion < question.length) {
            setAnswers((prevAnswers) => {
                const newAnswers = [...prevAnswers];
                newAnswers[currentQuestion] = option;
                return newAnswers;
            });

            setCurrentQuestion((prevQuestion) => prevQuestion + 1);

            if (currentQuestion === 0) {
                setQuestion1(question[0]);
            } else if (currentQuestion === 1) {
                setQuestion2(question[1]);
            } else if (currentQuestion === 2) {
                setQuestion3(question[2]);
            } else if (currentQuestion === 3) {
                setShowThankYou(true);
                setQuestion4(question[3]);
                saveFeedback();
            }
        }
    };

    return (
        <div>
            {new Date(currentDate) > new Date(eventDate) ?

                <div>
                    {showThankYou ?
                        <div>
                            <p className="bg-blue-500 px-4 py-3 text-white font-bold text-3xl">Thank you for your
                                feedback!</p>
                        </div>
                        :
                        <form>
                            {/* component */}
                            <div className="flex flex-col justify-center items-center mt-20 text-2xl">
                                <div className="bg-white rounded-md pb-2 shadow-lg mt-36">
                                    <p className="bg-blue-500 px-4 py-3 text-white font-bold rounded-t-md">{question[currentQuestion]}</p>
                                    <div className="flex flex-col w-full gap-3 pt-3 pb-2 px-2 relative">
                                        <div id="answer" className=" relative w-full h-8 p-3 rounded-md ">
                                            <ul>
                                                <li className="absolute top-[50%] left-3 text-gray-400   -translate-y-[50%] w-full text-left mx-0"
                                                    onClick={() => optionClicked("Satisfied")}>I am satisfied!
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="answer" className=" relative w-full h-8 p-3 rounded-md ">
                                            <ul>
                                                <li className="absolute top-[50%] left-3 text-gray-400   -translate-y-[50%] w-full text-left mx-0"
                                                    onClick={() => optionClicked("Neutral")}>Neutral
                                                </li>
                                            </ul>
                                        </div>
                                        <div id="answer" className=" relative w-full h-8 p-3 rounded-md ">
                                            <ul>
                                                <li className="absolute top-[50%] left-3 text-gray-400   -translate-y-[50%] w-full text-left mx-0"
                                                    onClick={() => optionClicked("Dissatisfied")}>I am dissatisfied!
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    }
                </div> :
                <div> Feedback Form is not available</div>}
        </div>
    )
}

export default FeedbackForm