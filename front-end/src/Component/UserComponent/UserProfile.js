import React, { useEffect, useRef, useState } from 'react';
import {
    Container,
    Box,
    Typography,
    Avatar,
    Snackbar,
    Grid,
    Paper,
    List,
    ListItem,
    Button,
    TextField,
    IconButton,
    InputLabel,
    Select,
    MenuItem,
    FormControl,
} from '@mui/material';
import axios from 'axios';
import defaultProfileImage from '../../Asset/black.jpg';
import AddIcon from '@mui/icons-material/Add';
import Menu from '../Menu';

const UserProfile = () => {
    const [user, setUser] = useState({
        id: '', // Add user id if available
        fullname: '',
        age: '',
        gender: '',
        email: '',
        phone: '',
        badgeList: [],
        eventList: [],
    });
    const [editUser, setEditUser] = useState({});
    const [hasUpdates, setHasUpdates] = useState(false);
    const inputFileRef = useRef(null);
    const [profileImagePath, setProfileImagePath] = useState(null);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const defaultProfileImage = 'default.jfif';

    useEffect(() => {
        fetchUser();
    }, []);

    useEffect(() => {
        if (hasUpdates) {
            fetchUser();
            setHasUpdates(false);
        }
    }, [hasUpdates]);

    const handleSnackbarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackbarOpen(false);
    };

    const showSnackbar = () => {
        setSnackbarOpen(true);
    };

    const fetchUser = async () => {
        try {
            const id = localStorage.getItem('id');
            const response = await axios.get(`http://localhost:8080/api/v1/auth/user-profile/${id}`);
            setUser(response.data);

            const filePath = response.data.profile;
            const parts = filePath.split(/\\+/);
            const filename = parts[parts.length - 1];

            const pictureResponse = await axios.get(`http://localhost:8080/api/v1/auth/images/${filename}`);

            if (pictureResponse.status === 200) {
                setProfileImagePath(filename);
            } else {
                console.error('Failed to fetch profile image:', pictureResponse.data.error);
                setProfileImagePath('/api/profile-images/default.jpg');
            }
        } catch (error) {
            console.log('Error fetching user data', error);
        }
    };

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setEditUser((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleFileChange = async (e) => {
        const file = e.target.files[0];
        const id = localStorage.getItem('id');

        if (file) {
            try {
                const formData = new FormData();
                formData.append('file', file);
                formData.append('id', id);

                const response = await axios.post('http://localhost:8080/api/v1/auth/upload-profile-pic', formData);

                if (response.status === 200) {
                    fetchUser();
                } else {
                    console.error('Failed to upload file');
                }
            } catch (error) {
                console.error('Error uploading file:', error);
            }
        }
    };

    const openFileInput = () => {
        inputFileRef.current.click();
    };

    const handleSave = async () => {
        try {
            await axios.patch(`http://localhost:8080/api/v1/auth/users/${user.id}`, editUser);
            fetchUser();
            showSnackbar();
        } catch (error) {
            console.error('Error updating user details', error);
        }
    };

    const getFilenameFromPath = (filePath) => {
        if (!filePath) {
            return 'default.jfif';
        }

        const parts = filePath.split(/\\+/);
        return parts[parts.length - 1];
    };

    return (
        <Container maxWidth="lg" sx={{ minHeight: '100vh', marginTop: '120px' }}>
            <Menu />
            <Grid container spacing={4}>
                <Grid item xs={12} sm={4}>
                    <Paper elevation={3} sx={{ p: 2, height: '570px' }}>
                        <Typography variant="h6" mb={2}>
                            Recent Events Registered
                        </Typography>
                        {user.eventList && user.eventList.length > 0 ? (
                            user.eventList.map((event, index) => {
                                // Add checks for truthiness of event and its properties
                                const filename = event && event.eventimages ? getFilenameFromPath(event.eventimages) : "default.jfif";

                                return (
                                    <Box key={index} mb={4} sx={{ display: 'flex', alignItems: 'center' }}>
                                        <img
                                            src={`http://localhost:8080/images/evimages/${filename}`}
                                            alt="Event"
                                            style={{ width: 100, height: 70, marginRight: 8 }}
                                        />
                                        <Box marginLeft={'10px'}>
                                            <Typography variant="subtitle1"><strong>{event && event.eventname}</strong></Typography>
                                            <Typography variant="body2">
                                                <strong>Venue:</strong> {event && event.eventvenue}
                                            </Typography>
                                            <Typography variant="body2">
                                                <strong>Date:</strong> {event && event.eventdate}
                                            </Typography>
                                        </Box>
                                    </Box>
                                );
                            })
                        ) : (
                            <Typography variant="body2">No recent events registered.</Typography>
                        )}
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Paper elevation={3} sx={{ p: 2, height: '570px' }}>
                        <Box display="flex" flexDirection="column" alignItems="center" mb={2}>
                            <div style={{ position: 'relative' }}>
                                <img
                                    src={`http://localhost:8080/api/v1/auth/images/${profileImagePath}`}
                                    alt="Profile"
                                    className="profile-image"
                                    style={{
                                        width: 70,
                                        height: 70,
                                        margin: '0 auto',
                                        borderRadius: '50%',
                                    }}
                                    onError={() => setProfileImagePath(defaultProfileImage)}
                                />
                                <IconButton
                                    sx={{
                                        position: 'absolute',
                                        bottom: 0,
                                        right: -10,
                                        background: 'lightgray',
                                    }}
                                    onClick={openFileInput}
                                >
                                    <AddIcon />
                                </IconButton>
                            </div>
                            <Typography variant="h6">{/* Remove user.fullname here */}</Typography>
                            <input
                                type="file"
                                accept="image/*"
                                ref={inputFileRef}
                                style={{ display: 'none' }}
                                onChange={handleFileChange}
                            />
                        </Box>
                        <List>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <ListItem>
                                        <TextField
                                            fullWidth
                                            label="Full Name" // Added field for Full Name
                                            name="fullname"
                                            value={editUser.fullname || user.fullname}
                                            onChange={handleInputChange}
                                        />
                                    </ListItem>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItem>
                                        <TextField
                                            fullWidth
                                            label="Email"
                                            name="email"
                                            value={user.email}
                                            InputProps={{ readOnly: true }}
                                        />
                                    </ListItem>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItem>
                                        <TextField
                                            fullWidth
                                            label="Phone"
                                            name="phone"
                                            autoComplete="tel"
                                            value={editUser.phone || user.phone}
                                            onChange={handleInputChange}
                                        />
                                    </ListItem>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItem>
                                        <FormControl fullWidth>
                                            <InputLabel id="gender-label">Gender</InputLabel>
                                            <Select
                                                labelId="gender-label"
                                                id="gender"
                                                name="gender"
                                                value={editUser.gender || user.gender}
                                                onChange={handleInputChange}
                                            >
                                                <MenuItem value="Male">Male</MenuItem>
                                                <MenuItem value="Female">Female</MenuItem>
                                                <MenuItem value="Other">Other</MenuItem>
                                            </Select>
                                        </FormControl>
                                    </ListItem>
                                </Grid>
                            </Grid>
                        </List>
                        <Box display="flex" justifyContent="center">
                            <Button variant="contained" color="primary" onClick={handleSave}>
                                Save
                            </Button>
                        </Box>
                        <Snackbar
                            open={snackbarOpen}
                            autoHideDuration={3000}
                            onClose={handleSnackbarClose}
                            message="User updated successfully"
                        />
                    </Paper>
                </Grid>
                <Grid item xs={12} sm={4}>
                    <Paper elevation={3} sx={{ p: 2, height: '570px' }}>
                        <Typography variant="h6" mb={2}>
                            Points and Badges
                        </Typography>
                        {user.point === null ? (
                            <Typography variant="body2">No points available.</Typography>
                        ) : (
                            <Typography variant="subtitle1">{`Points: ${user.point}`}</Typography>
                        )}
                        <Typography variant="subtitle1">Badges:</Typography>
                        {Array.isArray(user.badgeList) && user.badgeList.length > 0 ? (
                            user.badgeList.map((badge, index) => {
                                const filename = getFilenameFromPath(badge.image);
                                return (
                                    <Box key={index} sx={{ display: 'flex', alignItems: 'center', mt: 1 }}>
                                        <img
                                            src={`http://localhost:8080/api/v1/auth/images/badges/${filename}`}
                                            alt="Badge"
                                            style={{ width: 30, height: 30, marginRight: 8 }}
                                        />
                                        <Typography variant="body2">Earn {`${badge.name} at ${badge.pointsToEarn} points`}</Typography>
                                    </Box>
                                );
                            })
                        ) : (
                            <Typography variant="body2">No badges earned.</Typography>
                        )}
                    </Paper>
                </Grid>
            </Grid>
        </Container>
    );
};

export default UserProfile;
