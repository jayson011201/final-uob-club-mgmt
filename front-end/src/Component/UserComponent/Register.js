import React, {useEffect, useState} from 'react';
import {
    Box,
    Grid,
    Paper,
    Typography,
    TextField,
    Button,
    Link,
    FormControl,
    InputLabel,
    Select,
    MenuItem,
    Modal,
    IconButton,
    Alert,
    Snackbar,
    CircularProgress, // Import CircularProgress for loading indicator
} from '@mui/material';
import logo from '../../Asset/logo.png';
import { Facebook, Google } from '@mui/icons-material';
import { Link as RouterLink } from 'react-router-dom';
import axios from 'axios';
import CloseIcon from '@mui/icons-material/Close';
import {useGoogleLogin} from "@react-oauth/google";

function Register() {
    const [gender, setGender] = React.useState('');
    const [fullname, setFullname] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [activationCode, setActivationCode] = useState('');
    const [isErrorAlertOpen, setIsErrorAlertOpen] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [profileInfo, setProfileInfo] = useState([]);
    const isOpenPasswordModal = () => setOpenModal(true);
    const isClosePasswordModal = () => setOpenModal(false);
    const [openModal, setOpenModal] = useState(false);

    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [userInfo, setUserInfo] = useState([]);

    const handleRegistration = async () => {
        try {
            setIsLoading(true);

            const registrationData = {
                fullname,
                email,
                phone,
                gender,
                password,
                confirmPassword,
            };

            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/signup',
                registrationData
            );
            handleOpen();
        } catch (error) {
            setIsErrorAlertOpen(true);
            console.error('Registration error', error);
        } finally {
            setIsLoading(false);
        }
    };

    const handleActivation = async () => {
        handleClose();
        try {
            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/verify-otp',
                { code: activationCode }
            );

            if (response.status === 200) {
                window.location.href = '/login';
            } else {
                console.error('Invalid activation code');
            }
        } catch (error) {
            console.error('Activation error', error);
        }
    };

    const handleGoogleRegistration = async () => {
        try {
            setIsLoading(true);
            const registraterData = {
                email,
                password,
            };

            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/google-sign-up',
                registraterData
            );

            setOpenModal(false);
            handleOpen();
        } catch (error) {
            console.error('Registration error', error);
        }finally {
            setIsLoading(false);
        }
    };

    const modalStyle = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        boxShadow: 24,
        p: 4,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    };

    const handleGenderChange = (event) => {
        setGender(event.target.value);
    };

    const handleGoogleSignIn = async () => {
        try {
            await login();
        } catch (error) {
            console.error('Error initiating Google login:', error);
        }
    };

    const login = useGoogleLogin({
        onSuccess: (response) => {
            setUserInfo(response);
            isOpenPasswordModal();
        },
        onError: (error) => console.log(`Login Failed: ${error}`)
    });

    useEffect(
        () => {
            if (userInfo) {
                axios
                    .get(`https://www.googleapis.com/oauth2/v1/userinfo?access_token=${userInfo.access_token}`, {
                        headers: {
                            Authorization: `Bearer ${userInfo.access_token}`,
                            Accept: 'application/json'
                        }
                    })
                    .then((response) => {
                        setProfileInfo(response.data);
                        setEmail(response.data.email);
                    })
                    .catch((error) => console.log(error));
            }
        },
        [userInfo]
    );

    const handleResendActivation = async () => {
        try {
            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/resend-otp',
                { email: email }
            );

            if (response.status === 200) {
                console.log('Resend Otp Successful');
            } else {
                console.error('Resend Otp Failed');
            }
        } catch (error) {
            console.error('error !', error);
        }
    }

    return (
        <Grid
            container
            component="main"
            sx={{
                height: '100vh',
                backgroundColor: (theme) => theme.palette.primary.main,
            }}
        >
            <Grid
                item
                xs={false}
                sm={4}
                md={7}
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: 'white',
                    borderTopRightRadius: '200px',
                    borderBottomRightRadius: '200px',
                }}
            >
                <RouterLink to={'/'}>
                    <img
                        src={logo}
                        alt="Logo"
                        style={{ maxWidth: '80%', height: 'auto', marginLeft: '10%' }}
                    />
                </RouterLink>
            </Grid>

            <Grid
                item
                xs={12}
                sm={8}
                md={5}
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: (theme) => theme.palette.primary.main,
                    height: '100vh',
                }}
            >
                <Paper
                    elevation={6}
                    square
                    sx={{
                        my: 0,
                        mx: 4,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        height: 'auto',
                        width: '100%',
                        maxWidth: '400px',
                        p: 3,
                    }}
                >
                    <Typography component="h2" variant="h5" sx={{ fontWeight: 'bold' }}>
                        Register
                    </Typography>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="fullname"
                        label="Full Name"
                        name="fullname"
                        autoComplete="name"
                        autoFocus
                        value={fullname}
                        onChange={(e) => setFullname(e.target.value)}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                    />
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={7}>
                            <TextField
                                margin="normal"
                                required
                                fullWidth
                                id="phone"
                                label="Phone Number"
                                name="phone"
                                autoComplete="tel"
                                value={phone}
                                onChange={(e) => setPhone(e.target.value)}
                            />
                        </Grid>
                        <Grid item xs={12} sm={5}>
                            <FormControl fullWidth margin="normal">
                                <InputLabel id="gender-label">Gender</InputLabel>
                                <Select
                                    labelId="gender-label"
                                    id="gender"
                                    value={gender}
                                    label="Gender"
                                    onChange={handleGenderChange}
                                >
                                    <MenuItem value={'Female'}>Female</MenuItem>
                                    <MenuItem value={'Male'}>Male</MenuItem>
                                    <MenuItem value={'Other'}>Other</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                    </Grid>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="new-password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="confirmPassword"
                        label="Confirm Password"
                        type="password"
                        id="confirmPassword"
                        autoComplete="new-password"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleRegistration}
                        disabled={isLoading}
                    >
                        {isLoading ? <CircularProgress size={24} color="inherit" /> : 'Register'}
                    </Button>
                    <Typography component="p" variant="body2" sx={{ mt: 2 }}>
                        or
                    </Typography>
                    <Grid container justifyContent="center" sx={{ mt: 2 }}>
                        <Grid item>
                            <Button
                                variant="outlined"
                                startIcon={<Google />}
                                onClick={() => handleGoogleSignIn()}
                            >
                                Google
                            </Button>
                        </Grid>
                    </Grid>
                    <Typography component="p" variant="body2" sx={{ mt: 2 }}>
                        <Link href="/forgot-password" variant="body2">
                            {'Forgot Password?'}
                        </Link>
                        {'\u00A0|\u00A0'}
                        <Link href="/login" variant="body2">
                            {'Login'}
                        </Link>
                    </Typography>
                </Paper>
            </Grid>
            <Modal
                open={open}
                aria-labelledby="activation-code-modal"
                aria-describedby="activation-code-input"
            >
                <Box sx={modalStyle}>
                    <IconButton
                        onClick={handleClose}
                        aria-label="close"
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography id="activation-code-modal" variant="h6" component="h2">
                        Check email for activation code
                    </Typography>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        id="activation-code"
                        label="Activation Code"
                        name="activation"
                        autoComplete="off"
                        autoFocus
                        value={activationCode}
                        onChange={(e) => setActivationCode(e.target.value)}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleActivation}
                    >
                        Enter
                    </Button>
                    <Button
                        fullWidth
                        variant="text"
                        color="primary"
                        onClick={handleResendActivation}
                    >
                        Resend activation code
                    </Button>
                </Box>
            </Modal>

            <Snackbar
                open={isErrorAlertOpen}
                autoHideDuration={4000}
                onClose={() => setIsErrorAlertOpen(false)}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                elevation={3}
            >
                <Alert onClose={() => setIsErrorAlertOpen(false)} severity="error">
                    Email already registered...
                </Alert>
            </Snackbar>

            <Modal
                open={openModal}
                aria-labelledby="activation-code-modal"
                aria-describedby="activation-code-input"
            >
                <Box sx={modalStyle}>
                    <IconButton
                        onClick={isClosePasswordModal}
                        aria-label="close"
                        sx={{
                            position: 'absolute',
                            right: 8,
                            top: 8,
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                    <Typography id="activation-code-modal" variant="h6" component="h2">
                        Enter you account password
                    </Typography>
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="new-password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                    <TextField
                        margin="normal"
                        required
                        fullWidth
                        name="confirmPassword"
                        label="Confirm Password"
                        type="password"
                        id="confirmPassword"
                        autoComplete="new-password"
                        value={confirmPassword}
                        onChange={(e) => setConfirmPassword(e.target.value)}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        sx={{ mt: 3, mb: 2 }}
                        onClick={handleGoogleRegistration}
                        disabled={isLoading}
                    >
                        {isLoading ? <CircularProgress size={24} color="inherit" /> : 'Enter'}
                    </Button>
                </Box>
            </Modal>
        </Grid>
    );
}

export default Register;
