import React, { useState, useEffect } from 'react';
import { useParams, Link } from 'react-router-dom';
import axios from 'axios';
import {
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails,
    List,
    ListItem,
    ListItemText,
    Paper,
} from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Menu from "../Menu";

function ListEvent() {
    const [userId, setUserId] = useState(null)
    const [events, setEvents] = useState({
        currentEvents: [],
        upcomingEvents: [],
        pastEvents: [],
        registeredEvents: [],
    });
    const [tabValue, setTabValue] = useState(0);
    const [registrationInfo, setRegistrationInfo] = useState({});

    const renderRegistrationLink = async (event) => {
        try {
            const response = await axios.get(
                `http://localhost:8080/checkRegistration/${userId}/${event.eventid}`
            );

            setRegistrationInfo((prevState) => ({
                ...prevState,
                [event.eventid]: response.data,
            }));
        } catch (error) {
            console.error('Error checking registration:', error);
            setRegistrationInfo((prevState) => ({
                ...prevState,
                [event.eventid]: 'Error checking registration.',
            }));
        }
    };

    useEffect(() => {
        setUserId(localStorage.getItem("id"));
        const fetchEvents = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/events-categories/${userId}`);
                setEvents(response.data.body);
                response.data.body.upcomingEvents.forEach((event) => {
                    renderRegistrationLink(event);
                });
            } catch (error) {
                console.error('Error fetching events:', error);
            }
        };

        fetchEvents();
    }, [userId]);

    const handleChangeTab = (event, newValue) => {
        setTabValue(newValue);
    };

    const isAdmin = () => {
        const role = localStorage.getItem('role');
        return role === "ADMIN";
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column', textAlign: 'center', marginLeft: '100px', marginRight: '100px', marginTop: '120px' }}>
            <Menu/>
            <Typography variant="h5" gutterBottom>
                Event Viewer
            </Typography>

            <div style={{ display: 'flex', marginTop: '20px' }}>
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={tabValue}
                    onChange={handleChangeTab}
                    aria-label="Event categories"
                    sx={{ borderRight: 1, borderColor: 'divider', marginRight: 2, height: '100%', overflowY: 'auto' }}
                >
                    <Tab label="Today's Events" {...a11yProps(0)} />
                    <Tab label="Upcoming Events" {...a11yProps(1)} />
                    <Tab label="Past Events" {...a11yProps(2)} />
                    <Tab label="Registered Events" {...a11yProps(3)} />
                </Tabs>

                <div style={{ flexGrow: 1, padding: 20, height: '100%', overflowY: 'auto' }}>
                    {events[tabValue === 0 ? 'currentEvents' : tabValue === 1 ? 'upcomingEvents' : tabValue === 2 ? 'pastEvents' : 'registeredEvents'].map((event, index) => (
                        <Accordion key={event.eventid}>
                            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                                <Typography variant="h6">{event.eventname}</Typography>
                            </AccordionSummary>
                            <AccordionDetails>
                                <Paper style={{ padding: '20px', width: '96%' }}>
                                    <List>
                                        <ListItem>
                                            <ListItemText primary={`Event Name: ${event.eventname}`} />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText primary={`Event Date: ${event.eventdate}`} />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText primary={`Event Time: ${event.eventtime}`} />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText primary={`Event Venue: ${event.eventvenue}`} />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText primary={`Event Description: ${event.eventdesc}`} />
                                        </ListItem>
                                        {/*{tabValue === 3 && (*/}
                                        {/*    <ListItem>*/}
                                        {/*        <ListItemText primary={`Registration Status: ${event.registrationStatus}`} />*/}
                                        {/*    </ListItem>*/}
                                        {/*)}*/}
                                        {registrationInfo[event.eventid] && (
                                            <Typography variant="body1" gutterBottom style={{ marginTop: '10px' }}>
                                                {registrationInfo[event.eventid]}
                                            </Typography>
                                        )}
                                        {tabValue === 1 && !registrationInfo[event.eventid] && (
                                            <span style={{ marginLeft: '10px' }}>
                      To register for this event, click{' '}
                                                <Link to={isAdmin() ? `/admin/register/${event.eventid}` : `/member/register/${event.eventid}`}>here</Link>
                                                {/*<Link to={`/member/register/${event.eventid}`}>here</Link>.*/}
                    </span>
                                        )}
                                    </List>
                                </Paper>
                            </AccordionDetails>
                            {index !== events[tabValue === 0 ? 'currentEvents' : tabValue === 1 ? 'upcomingEvents' : tabValue === 2 ? 'pastEvents' : 'registeredEvents'].length - 1 && (
                                <div style={{ height: '10px' }}></div>
                            )}
                        </Accordion>
                    ))}
                </div>
            </div>
        </div>
    );
}

function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export default ListEvent;
