import React, { useState } from 'react';
import { Paper, Grid, Typography, Button } from '@mui/material';
import { Flipper, Flipped } from 'react-flip-toolkit';
import { styled } from '@mui/system';

const EventPaper = styled(Paper)(({ theme }) => ({
    marginBottom: theme.spacing(3),
}));

const EventDetails = styled('div')(({ theme }) => ({
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
}));

const divStyle = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: 'linear-gradient(to bottom, #cee6fa, #f4f8fb)',
};

const Mission = () => {
    const [isMissionView, setMissionView] = useState(true);

    const toggleView = () => {
        setMissionView(!isMissionView);
    };

    return (
        <div style={divStyle}>
            <Flipper flipKey={isMissionView}>
                <Grid container spacing={2} alignItems="center" marginBottom={'80px'}>
                    <Grid item xs={12} md={12} marginX="10%">
                        <EventDetails>
                            <Flipped flipId="event-title">
                                <Typography variant="h3" component="div" className="card-title" style={{ fontSize: '4.0rem', marginBottom: '1rem', fontFamily: 'cursive' }}>
                                    {isMissionView ? 'Our Mission' : 'What We Do'}
                                </Typography>
                            </Flipped>
                            <Flipped flipId="event-text">
                                <Typography variant="h6" component="div" className="card-text" style={{ fontSize: '1.8rem', marginBottom: '1rem', lineHeight: '1.6', fontFamily: 'cursive' }}>
                                    {isMissionView
                                        ? 'Our mission is clear: to bring students and faculty members together through a wide range of exciting events and activities. We are committed to providing a platform where individuals from diverse backgrounds can come together, share their experiences, and build lasting friendships.'
                                        : '1NITY is your gateway to a world of engaging events and initiatives. From cultural festivals that celebrate the rich tapestry of our university\'s community to academic seminars that promote knowledge sharing, we have something for everyone. Whether you\'re a freshman looking to make new friends or a senior seeking opportunities to give back, 1NITY has a place for you.'}
                                </Typography>
                            </Flipped>
                            <Flipped flipId="event-button">
                                <Button onClick={toggleView} variant="outlined" color="primary" style={{ fontSize: '1.6rem', marginTop: '50px' }}>
                                    {isMissionView ? 'Check Out What We Do >>' : 'Check Out Our Mission >>'}
                                </Button>
                            </Flipped>
                        </EventDetails>
                    </Grid>
                </Grid>
            </Flipper>
        </div>
    );
};

export default Mission;
