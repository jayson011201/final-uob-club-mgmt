import React, { useState } from 'react';
import { Paper, Button, Container, Typography, Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const faqs = [
    {
        question: "What is your return policy?",
        answer: "Our return policy allows for returns up to 30 days after purchase. Items must be in new and unused condition with all original packaging and tags attached."
    },
    {
        question: "How can I track my order?",
        answer: "To track your order, please visit our Order Tracking page and enter your order number and email address. You will receive updates on your order status."
    },
    {
        question: "Do you offer international shipping?",
        answer: "Yes, we offer international shipping to many countries. Please check our Shipping & Delivery page for more information on international shipping rates and delivery times."
    },
    {
        question: "What payment methods do you accept?",
        answer: "We accept a variety of payment methods, including credit and debit cards, PayPal, and Apple Pay. For a full list of accepted payment options, visit our Payment Methods page."
    },
    {
        question: "How do I reset my password?",
        answer: "To reset your password, go to the login page and click on the 'Forgot Password' link. You will receive an email with instructions on how to reset your password."
    },
];

function Faq() {
    const [expanded, setExpanded] = useState(-1);

    const toggleAnswer = (index) => {
        if (index === expanded) {
            setExpanded(-1);
        } else {
            setExpanded(index);
        }
    };

    const divStyle = {
        justifyContent: 'center',
        height: 'auto',
        alignItems: 'center',
        background: 'linear-gradient(to bottom, #cee6fa, #f4f8fb)',
        paddingBottom: '80px',
    };

    return (
        <div style={divStyle}>
            <div style={{paddingRight: '10%', paddingLeft: '10%'}}>
                <div className="text-center" style={{marginBottom: '50px'}}>
                    <Typography variant="h2" className="mb-3" fontFamily='cursive' textAlign='left'>Frequently Asked Questions</Typography>
                </div>
                {faqs.map((faq, index) => (
                    <Accordion
                        key={index}
                        expanded={expanded === index}
                        onChange={() => toggleAnswer(index)}
                        sx={{
                            marginY: '20px',
                            borderRadius: '10px', // Set the border radius here
                        }}>
                        <AccordionSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls={`answer-${index}`}
                            id={`faq-${index}`}
                            sx={{
                                borderRadius: '10px',
                                backgroundColor: '#3d8ed0', // Set the background color here
                                color: '#ffffff', // Set the text color here
                            }}
                        >
                            <Typography variant="h6" fontFamily='cursive' fontSize='24px'>{faq.question}</Typography>
                        </AccordionSummary>
                        <AccordionDetails sx={{ color: '#555', backgroundColor: '#f4f8fb', borderRadius: '10px' }}> {/* Set the text color for AccordionDetails */}
                            <Typography variant="body1" fontFamily='cursive' fontSize='18px'>{faq.answer}</Typography>
                        </AccordionDetails>
                    </Accordion>
                ))}
            </div>
        </div>
    );
}

export default Faq;
