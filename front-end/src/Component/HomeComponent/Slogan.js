import React from 'react';
import { Paper, Grid, Typography, Button } from '@mui/material';
import { styled } from '@mui/system';
import home1 from '../../Asset/home1.jpg';

const EventPaper = styled(Paper)(({ theme }) => ({
    marginBottom: theme.spacing(3),
}));

const EventImage = styled('img')({
    maxWidth: '100%',
});

const EventDetails = styled('div')(({ theme }) => ({
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
}));

function Slogan({ event }) {
    const divStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // height: '40vh',
        background: 'linear-gradient(to bottom, #f4f8fb, #cee6fa)', // Adjust the colors as needed
    };

    return (
        <div style={divStyle}>
            <Grid container spacing={2} alignItems="center" marginX={'10%'} marginBottom={'80px'}>
                <Grid item xs={12} md={12}>
                    <EventDetails>
                        <Typography variant="h3" component="div" className="card-title" style={{ fontSize: '2.5rem', fontWeight: 'bold', marginBottom: '1rem', fontFamily: 'cursive' }}>
                            Our Slogan: Enhancing Connectivity, Fostering Unity
                        </Typography>
                        <Typography variant="h6" component="div" className="card-text" style={{ fontSize: '1.8rem', marginBottom: '1rem', lineHeight: '1.6', fontFamily: 'cursive', textAlign: 'center' }}>
                            Our slogan encapsulates our core values. We believe that by enhancing connectivity, we pave the way for understanding and collaboration among all members of the Universiti Malaya community. Through these connections, we can foster a sense of unity that transcends differences and creates a stronger, more inclusive campus.
                        </Typography>
                        <Button href='/register' variant="outlined" color="primary" style={{ fontSize: '1.6rem', marginTop: '50px' }}>
                            REGISTER BECOME OUR MEMBER >>
                        </Button>
                    </EventDetails>
                </Grid>
            </Grid>
        </div>
    );
}

export default Slogan;
