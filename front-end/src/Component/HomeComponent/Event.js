import React, { useState, useEffect } from 'react';
import { Paper, Grid, Typography, Button } from '@mui/material';
import { styled } from '@mui/system';
import image1 from '../../Asset/event1.jfif';
import image2 from '../../Asset/event2.jfif';
import image3 from '../../Asset/event3.jfif';

const EventPaper = styled(Paper)(({ theme }) => ({
    marginBottom: theme.spacing(3),
}));

const EventImage = styled('img')({
    maxWidth: '100%',
    border: '5px solid #3d8ed0', // Add border style
    borderRadius: '20px', // Add border radius
});

const EventDetails = styled('div')(({ theme }) => ({
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
}));

const ImageContainer = styled('div')({
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%', // Adjust the width as needed
    marginTop: '20px', // Add margin to separate the images from the text
});

function Event({ event }) {
    const [userId, setUserId] = useState(null);
    const [userRole, setUserRole] = useState(null); // Initialize userRole state

    const divStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'linear-gradient(to bottom, #f4f8fb, #cee6fa)',
    };

    useEffect(() => {
        const storedUserId = localStorage.getItem('id');
        const storedUserRole = localStorage.getItem('role'); // Retrieve user role

        if (storedUserId && storedUserRole) {
            setUserId(storedUserId);
            setUserRole(storedUserRole); // Set user role state
        }
    }, []);

    return (
        <div style={divStyle}>
            <Grid container spacing={2} alignItems="center" marginX={'10%'} marginBottom={'80px'}>
                <Grid item xs={12} md={12}>
                    <EventDetails>
                        <Typography variant="h3" component="div" className="card-title" style={{ fontSize: '4.0rem', marginBottom: '1rem', fontFamily: 'cursive' }}>
                            Our Events
                        </Typography>
                        <Typography variant="h6" component="div" className="card-text" style={{ fontSize: '1.8rem', marginBottom: '1rem', lineHeight: '1.6', fontFamily: 'cursive', textAlign: 'center' }}>
                            Our slogan encapsulates our core values. We believe that by enhancing connectivity, we pave the way for understanding and collaboration among all members of the Universiti Malaya community. Through these connections, we can foster a sense of unity that transcends differences and creates a stronger, more inclusive campus.
                        </Typography>
                        <ImageContainer>
                            <EventImage src={image1} />
                            <EventImage src={image2} />
                            <EventImage src={image3} />
                        </ImageContainer>
                        <Button href={userRole === 'ADMIN' ? `/admin/list-event/${userId}` : `/member/list-event/${userId}`} variant="outlined" color="primary" style={{ fontSize: '1.6rem', marginTop: '50px' }}>
                            See more events >>
                        </Button>
                    </EventDetails>
                </Grid>
            </Grid>
        </div>
    );
}

export default Event;
