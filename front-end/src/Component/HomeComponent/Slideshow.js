// Slideshow.js
import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import Paper from '@mui/material/Paper';
import Typography from '@mui/material/Typography';
import Logo from '../../Asset/logo.png';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import '../../CSS/Slideshow.css'; // Make sure your CSS is adjusted for Material-UI components

function Slideshow() {
    const divStyle = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh', // Adjust the height as needed
        background: 'linear-gradient(to bottom, #6babe0, #f4f8fb)', // Adjust the colors as needed
    };

    return (
        <div style={divStyle}>
            <img src={Logo} alt="Logo" />
        </div>
    );
}

export default Slideshow;
