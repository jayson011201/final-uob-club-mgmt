import React, { useState, useEffect } from 'react';
import { Paper, TableContainer, Table, TableHead, TableBody, TableRow, TableCell, Button, Modal, IconButton } from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';
import CreateUserModal from './UserManagementComponent/CreateUserModal';
import EditUserModal from './UserManagementComponent/EditUserModal';
import DeleteUserModal from './UserManagementComponent/DeleteUserModal';
import UserDetailModal from './UserManagementComponent/UserDetailModal';
import axios from 'axios';
import AdminMenu from "./AdminMenu";

function UserManagement() {
    const [isMobileView, setIsMobileView] = useState(window.innerWidth < 600);
    const [selectedUser, setSelectedUser] = useState(null);

    const [isCreateUserModalOpen, setCreateUserModalOpen] = useState(false);
    const [isEditUserModalOpen, setEditUserModalOpen] = useState(false);
    const [isDeleteUserModalOpen, setDeleteUserModalOpen] = useState(false);
    const [isUserDetailModalOpen, setUserDetailModalOpen] = useState(false);

    const [users, setUsers] = useState([]);
    const [hasUpdates, setHasUpdates] = useState(false); // Track updates

    const refreshUser = () => {
        setHasUpdates(true); // Set updates flag
    };

    useEffect(() => {
        fetchUsers(); // This fetches users when the component mounts
    }, []);


    useEffect(() => {
        if (hasUpdates) {
            fetchUsers();
            setHasUpdates(false); // Reset the updates flag after fetching
        }
    }, [hasUpdates]);

    const fetchUsers = async () => {
        try {
            const response = await axios.get('http://localhost:8080/api/v1/auth/users');
            setUsers(response.data);
        } catch (error) {
            console.log('Error fetching users data', error);
        }
    }

    useEffect(() => {
        const handleResize = () => {
            setIsMobileView(window.innerWidth < 600);
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const contentStyle = isMobileView ? { marginLeft: '16px', marginRight: '16px' } : { marginLeft: '300px', marginRight: '80px', marginTop: '30px' };

    const handleOpenDeleteModal = (user) => {
        setSelectedUser(user);
        setDeleteUserModalOpen(true);
        refreshUser(); // Call refreshUser to fetch updated data
    };

    const handleOpenDetailModal = (user) => {
        setSelectedUser(user);
        setUserDetailModalOpen(true);
        //refreshUser(); // Call refreshUser to fetch updated data
    };

    const handleOpenEditModal = (user) => {
        setSelectedUser(user);
        setEditUserModalOpen(true);
        refreshUser(); // Call refreshUser to fetch updated data
    };

    const handleCloseModal = () => {
        setSelectedUser(null);
    };

    return (
        <div style={contentStyle}>
            <AdminMenu/>
            <h1 style={{fontSize: '35px', fontWeight: 'bold', marginBlock: '10px'}}>User Management</h1>
            <Button variant="contained" onClick={() => setCreateUserModalOpen(true)}>
                Add User
            </Button>
            <br />
            <br />
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650, overflowX: 'auto' }} aria-label="simple table">
                    <TableHead sx={{ backgroundColor: '#eeeeee' }}>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Name</TableCell>
                            <TableCell align="left">Email</TableCell>
                            <TableCell align="left">Role</TableCell>
                            <TableCell align="left">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {users.map((user) => (
                            <TableRow
                                key={user.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {user.id}
                                </TableCell>
                                <TableCell align="left">{user.fullname}</TableCell>
                                <TableCell align="left">{user.email}</TableCell>
                                <TableCell align="left">{user.role}</TableCell>
                                <TableCell align="left">
                                    <IconButton onClick={() => handleOpenEditModal(user)}>
                                        <EditIcon />
                                    </IconButton>
                                    <IconButton onClick={() => handleOpenDeleteModal(user)}>
                                        <DeleteIcon />
                                    </IconButton>
                                    <IconButton onClick={() => handleOpenDetailModal(user)}>
                                        <VisibilityIcon />
                                    </IconButton>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <CreateUserModal
                isOpen={isCreateUserModalOpen}
                onClose={() => setCreateUserModalOpen(false)}
                onUserCreated={() => refreshUser()} // Pass a callback function
                onCreate={(userData) => {
                    setCreateUserModalOpen(false);
                }}
            />

            <EditUserModal
                isOpen={isEditUserModalOpen}
                onClose={() => setEditUserModalOpen(false)}
                onUserEdited={() => refreshUser()} // Add an event handler if needed
                selectedUser={selectedUser} // Pass the selected user data
            />

            <DeleteUserModal
                isOpen={isDeleteUserModalOpen}
                onClose={() => setDeleteUserModalOpen(false)}
                onUserDeleted={() => refreshUser()} // Add an event handler if needed
                selectedUser={selectedUser} // Pass the selected user data
            />

            <UserDetailModal
                isOpen={isUserDetailModalOpen}
                onClose={() => setUserDetailModalOpen(false)}
                selectedUser={selectedUser}
            />
        </div>
    );
}

export default UserManagement;
