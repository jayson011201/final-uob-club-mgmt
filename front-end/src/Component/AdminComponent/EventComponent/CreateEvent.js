import React, {useRef, useState} from 'react';
import { Button, TextField, Paper, Grid, Typography } from '@mui/material';
import axios from 'axios';


function CreateEvent() {
    const [selectedImage, setSelectedImage] = useState(null);

    const handleImageSelect = (e) => {
        const file = e.target.files[0];
        setSelectedImage(file);
    };

    const [event, setEvent] = useState({
        eventname: '',
        eventdesc: '',
        eventtime: '',
        eventdate: '',
        eventvenue: '',
        eventcapacity: '',
        eventimages: '',
        registerbefore: '',
        adminid: '',
        attendanceLink: '',
    });

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setEvent({ ...event, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const formData = new FormData();
            formData.append('file', selectedImage);
            formData.append('eventname', event.eventname);
            formData.append('eventdesc', event.eventdesc);
            formData.append('eventtime', event.eventtime);
            formData.append('eventdate', event.eventdate);
            formData.append('eventvenue', event.eventvenue);
            formData.append('eventcapacity', event.eventcapacity);
            formData.append('registerbefore', event.registerbefore);
            formData.append('adminid', localStorage.getItem("email"));

            // Make a POST request to upload the image
            const response = await axios.post('http://localhost:8080/create-event', formData);
            alert("Successfully created event");

            console.log(response.data);
            const createdEventID = response.data.eventid;
            // setEventID(createdEventID);
            const attendanceLink = `http://localhost:3000/attendance-form/${createdEventID}`;
            const updatedEvent = { ...event, attendanceLink };
            setEvent(updatedEvent);
            await axios.put(`http://localhost:8080/update-link/${createdEventID}`, updatedEvent);
            // Handle the response as needed
        } catch (error) {
            console.error('Error creating event:', error);
            alert('Error creating event');
        }
    };

    const disableScrollStyle = {
        overflow: 'hidden',
    };

    return (

        <div style={{ textAlign: 'center', marginTop: '50px', disableScrollStyle}}>
            <Typography variant="h5" gutterBottom>
                Create an Event:
            </Typography>

            <Paper elevation={10} style={{ padding: '20px', maxWidth: '80%', margin: 'auto', marginTop: '50px', marginBottom: '50px' }}>
                <form onSubmit={handleSubmit}>
                    <Grid container spacing ={3} style={{ display: 'flex' }}>
                        <Grid item xs={12} sm={6}>
                            <Typography variant="subtitle1">Event Name</Typography>
                            <TextField
                                name="eventname"
                                value={event.eventname}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'
                            />
                            <Typography variant="subtitle1">Event Time</Typography>
                            <TextField
                                name="eventtime"
                                value={event.eventtime}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'
                            />
                            <Typography variant="subtitle1">Event Date</Typography>
                            <TextField
                                name="eventdate"
                                value={event.eventdate}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'

                            />
                            <Typography variant="subtitle1">Last Day to Register</Typography>
                            <TextField
                                name="registerbefore"
                                value={event.registerbefore}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'
                            />
                            <Typography variant="subtitle1">Admin ID</Typography>
                            <TextField
                                name="adminid"
                                value={localStorage.getItem("email")}
                                onChange={handleInputChange}
                                required
                                aria-readonly={"true"}
                                fullWidth
                                margin='normal'
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Typography variant="subtitle1">Event Description</Typography>
                            <TextField
                                name="eventdesc"
                                value={event.eventdesc}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'
                                multiline
                                rows={6}
                            />
                            <Typography variant="subtitle1">Event Venue</Typography>
                            <TextField
                                name="eventvenue"
                                value={event.eventvenue}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'
                            />
                            <Typography variant="subtitle1">Event Capacity</Typography>
                            <TextField
                                name="eventcapacity"
                                value={event.eventcapacity}
                                onChange={handleInputChange}
                                required
                                fullWidth
                                margin='normal'
                            />
                            <Typography variant="subtitle1">Event Images</Typography>
                            <input
                                type="file"
                                name="file"
                                accept="image/*"
                                onChange={handleImageSelect}
                                required
                                fullWidth
                                margin="normal"
                            />
                        </Grid>
                    </Grid>


                    <br></br>
                    <br></br>
                    {/* Add similar TextField components for other event properties */}
                    <Button type="submit" variant="contained" color="primary">
                        Create Event
                    </Button>
                </form>
            </Paper>
        </div>
    );
}

export default CreateEvent;