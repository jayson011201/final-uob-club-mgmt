import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {useParams, useNavigate} from 'react-router-dom';
import {
    Button,
    TextField,
    Paper,
    Grid,
    Typography,
} from '@mui/material';
import AdminMenu from "../AdminMenu";

function EditEvent() {
    const {id} = useParams();
    let {eventId} = useParams();
    const navigate = useNavigate();

    const [event, setEvent] = useState({
        eventname: '',
        eventdesc: '',
        eventdate: '',
        eventtime: '',
        eventvenue: '',
        eventcapacity: '',
        registerbefore: '',
        adminid: '',
    });

    useEffect(() => {
        const fetchEvent = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/events/${eventId}`);
                console.log('Event details:', response.data);
                setEvent(response.data.body || {});
            } catch (error) {
                console.error('Error fetching event details:', error);
            }
        };

        fetchEvent();
    }, [eventId]);

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setEvent((prevEvent) => ({
            ...prevEvent,
            [name]: value,
        }));
    };

    const handleFormSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await axios.patch(`http://localhost:8080/update-event/${eventId}`, event);
        } catch (error) {
            console.error('Error updating event:', error);
        }
    };

    const handleDelete = async () => {
        try {
            const response = await axios.delete(`http://localhost:8080/delete-event/${eventId}`);
            navigate('/admin/event');
        } catch (error) {
            console.error('Error deleting event:', error);
        }
    };

    return (
        <div style={{marginTop: '50px', marginLeft: '300px'}}>
            <AdminMenu/>
            <div style={{textAlign: 'center'}}>
                <Typography variant="h5" gutterBottom>
                    Edit Event:
                </Typography>

                <Paper elevation={10} style={{
                    padding: '20px',
                    maxWidth: '80%',
                    margin: 'auto',
                    marginTop: '50px',
                    marginBottom: '50px'
                }}>
                    <form onSubmit={handleFormSubmit}>
                        <Grid container spacing={3} style={{display: 'flex'}}>
                            <Grid item xs={12} sm={6}>
                                <Typography variant="subtitle1">Event Name</Typography>
                                <TextField
                                    name="eventname"
                                    value={event.eventname}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                />
                                <Typography variant="subtitle1">Event Time</Typography>
                                <TextField
                                    name="eventtime"
                                    value={event.eventtime}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                />
                                <Typography variant="subtitle1">Event Date</Typography>
                                <TextField
                                    name="eventdate"
                                    value={event.eventdate}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                />
                                <Typography variant="subtitle1">Last Day to Register</Typography>
                                <TextField
                                    name="registerbefore"
                                    value={event.registerbefore}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                />
                                <Typography variant="subtitle1">Admin ID</Typography>
                                <TextField
                                    name="adminid"
                                    value={localStorage.getItem("email")}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                    aria-readonly={true}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <Typography variant="subtitle1">Event Description</Typography>
                                <TextField
                                    name="eventdesc"
                                    value={event.eventdesc}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                    multiline
                                    rows={6}
                                />
                                <Typography variant="subtitle1">Event Venue</Typography>
                                <TextField
                                    name="eventvenue"
                                    value={event.eventvenue}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                />
                                <Typography variant="subtitle1">Event Capacity</Typography>
                                <TextField
                                    name="eventcapacity"
                                    value={event.eventcapacity}
                                    onChange={handleInputChange}
                                    required
                                    fullWidth
                                    margin='normal'
                                />
                            </Grid>
                        </Grid>

                        <br></br>
                        <br></br>
                        <Button type="submit" variant="contained" color="primary">
                            Update Event
                        </Button>
                        <Button onClick={handleDelete} variant="contained" color="secondary"
                                style={{marginLeft: '10px'}}>
                            Delete Event
                        </Button>
                    </form>
                </Paper>
            </div>
        </div>
    );
}

export default EditEvent;
