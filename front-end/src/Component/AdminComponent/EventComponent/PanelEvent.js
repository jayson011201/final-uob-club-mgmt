import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {Typography, List, ListItem, Button, Paper, TableCell, TableRow, TableHead, Table, TableBody, TableContainer} from '@mui/material';

function PanelEvent() {
  const [events, setEvents] = useState([]);
  const [profileImagePath, setProfileImagePath] = useState(null);

  useEffect(() => {
    const fetchEvents = async () => {
      try {
        const response = await axios.get('http://localhost:8080/events');
        console.log('Response data:', response.data);
        setEvents(response.data.body || []);
        const path = response.data.body.eventimages;
        const parts = path.split(/\\+/);
        setProfileImagePath(parts[parts.length - 1]);
      } catch (error) {
        console.error('Error fetching events:', error);
      }
    };

    fetchEvents();
  }, []);

  const getFilenameFromPath = (filePath) => {
    if (!filePath) {
      return "default.jfif"; // or handle the default case as appropriate
    }

    const parts = filePath.split(/\\+/);
    return parts[parts.length - 1];
  };

  return (
      <div style={{textAlign: 'center', marginTop: '50px', marginRight: '50px'}}>
        <Typography variant="h6" gutterBottom>
          All Events
        </Typography>
        <Paper elevation={3} style={{padding: '20px'}}>
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Event Name</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Date</TableCell>
                  <TableCell>Time</TableCell>
                  <TableCell>Venue</TableCell>
                  <TableCell>Capacity</TableCell>
                  <TableCell>Register Before</TableCell>
                  <TableCell>Admin ID</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {events.map((event) => (
                    <TableRow key={event.eventid}>
                      <TableCell>{event.eventname}</TableCell>
                      <TableCell>{event.eventdesc}</TableCell>
                      <TableCell>{event.eventdate}</TableCell>
                      <TableCell>{event.eventtime}</TableCell>
                      <TableCell>{event.eventvenue}</TableCell>
                      <TableCell>{event.eventcapacity}</TableCell>
                      <TableCell>{event.registerbefore}</TableCell>
                      <TableCell>{event.adminid}</TableCell>
                      <TableCell>
                        <Link to={`/admin/edit-event/${event.eventid}`}>
                          <Button variant="outlined" color="primary" size="small" sx={{mr: 1}}>
                            Edit Event
                          </Button>
                        </Link>
                        <Link to={`/admin/event-details/${event.eventid}`}>
                          <Button variant="outlined" color="secondary" size="small">
                            View Details
                          </Button>
                        </Link>
                      </TableCell>
                    </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
  );
}

export default PanelEvent;