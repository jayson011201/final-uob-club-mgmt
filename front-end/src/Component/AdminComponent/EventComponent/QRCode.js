// Filename - App.js

import { useState } from 'react';
import QRCode from 'react-qr-code';

function QRCodes() {
	const [value, setValue] = useState();
	const [back, setBack] = useState('#FFFFFF');
	const [fore, setFore] = useState('#000000');
	const [size, setSize] = useState(256);

	return (
	
					<QRCode
						title="GeeksForGeeks"
						value="http://localhost:3000/attendance-form/1"
						bgColor={back}
						fgColor={fore}
						size={size === '' ? 0 : size}
					/>
	
	);
}

export default QRCodes;
