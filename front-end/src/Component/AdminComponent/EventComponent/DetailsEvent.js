import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Typography, Paper, Button, Grid } from '@mui/material';
import QRCode from "react-qr-code";
import AdminMenu from "../AdminMenu";

function DetailsEvent() {
  const {id} = useParams();
  const eventId = id; // Extract event ID from the URL parameter
  const [eventDetails, setEventDetails] = useState({
    eventname: '',
    eventdesc: '',
    eventdate: '',
    eventtime: '',
    eventvenue: '',
    eventcapacity: '',
    registerbefore: '',
    attendanceLink: '',
    adminid: '',
  });

  useEffect(() => {
    const fetchEvent = async () => {
      try {
        const response = await axios.get(`http://localhost:8080/events/${eventId}`);
        setEventDetails(response.data.body || {});
      } catch (error) {
        console.error('Error fetching event details:', error);
      }
    };

    fetchEvent();
  }, [eventId]);

  const handlePrint = () => {
    // Implement code to print event details
    window.print();
  };

  return (
      <Grid container justifyContent="center" alignItems="center" height="100vh" marginLeft="120px">
        <AdminMenu/>
        <Grid item xs={12} sm={8} md={6} lg={4}>
          <div style={{textAlign: 'center', marginTop: '50px'}}>
            <Typography variant="h4" gutterBottom>
              Event Details
            </Typography>
          </div>

          <Paper elevation={3} style={{padding: '20px', marginBottom: '20px'}}>

            {Object.keys(eventDetails).length > 0 ? (
                <>
                  <Typography variant="subtitle1">Event ID: {eventId}</Typography>
                  <Typography variant="subtitle1">Event Name: {eventDetails.eventname}</Typography>
                  <Typography variant="subtitle1">Event Description: {eventDetails.eventdesc}</Typography>
                  <Typography variant="subtitle1">Event Date: {eventDetails.eventdate}</Typography>
                  {/* Add more details as needed */}
                </>
            ) : (
                <Typography variant="body1">Loading event details...</Typography>
            )}
          </Paper>
          <Typography variant="body1">QR CODE:</Typography>
          <QRCode className="mx-auto"
              title="Attendance Link"
              value={eventDetails.attendanceLink}
          />
        </Grid>
      </Grid>
  );
}

export default DetailsEvent;