import React, {useState, useEffect} from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Button,
    Modal,
    IconButton
} from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';
import CreateBadgeModal from "./BadgeManagement/CreateBadgeModal";
import EditBadgeModal from "./BadgeManagement/EditBadgeModal";
import DeleteBadgeModal from "./BadgeManagement/DeleteBadgeModal";
import AdminMenu from "./AdminMenu";

function BadgeManagement() {
    const [isMobileView, setIsMobileView] = useState(window.innerWidth < 600);
    const [selectedBadge, setSelectedBadge] = useState(null);

    const [isCreateBadgeModalOpen, setCreateBadgeModalOpen] = useState(false);
    const [isEditBadgeModalOpen, setEditBadgeModalOpen] = useState(false);
    const [isDeleteBadgeModalOpen, setDeleteBadgeModalOpen] = useState(false);

    const [badges, setBadges] = useState([]);
    const [hasUpdates, setHasUpdates] = useState(false); // Track updates

    const refreshBadge = () => {
        setHasUpdates(true); // Set updates flag
    };

    useEffect(() => {
        fetchBadges(); // This fetches users when the component mounts
    }, []);


    useEffect(() => {
        if (hasUpdates) {
            fetchBadges();
            setHasUpdates(false); // Reset the updates flag after fetching
        }
    }, [hasUpdates]);

    const fetchBadges = async () => {
        try {
            const response = await axios.get('http://localhost:8080/api/v1/auth/badges');
            setBadges(response.data);
        } catch (error) {
            console.log('Error fetching badges data', error);
        }
    }

    useEffect(() => {
        const handleResize = () => {
            setIsMobileView(window.innerWidth < 600);
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const contentStyle = isMobileView ? {marginLeft: '16px', marginRight: '16px'} : {
        marginLeft: '300px',
        marginRight: '80px',
        marginTop: '30px'
    };

    const handleOpenDeleteModal = (badge) => {
        setSelectedBadge(badge);
        setDeleteBadgeModalOpen(true);
        refreshBadge(); // Call refreshUser to fetch updated data
    };

    const handleOpenEditModal = (badge) => {
        setSelectedBadge(badge);
        setEditBadgeModalOpen(true);
        refreshBadge(); // Call refreshUser to fetch updated data
    };

    const handleCloseModal = () => {
        setSelectedBadge(null);
    };

    const getFilenameFromPath = (filePath) => {
        const parts = filePath.split(/\\+/);
        return parts[parts.length - 1];
    };

    return (
        <div style={contentStyle}>
            <AdminMenu/>
            <h1 style={{fontSize: '35px', fontWeight: 'bold', marginBlock: '10px'}}>Badge Management</h1>
            <Button variant="contained" onClick={() => setCreateBadgeModalOpen(true)}>
                Add Badge
            </Button>
            <br/>
            <br/>
            <TableContainer component={Paper}>
                <Table sx={{minWidth: 650, overflowX: 'auto'}} aria-label="simple table">
                    <TableHead sx={{backgroundColor: '#eeeeee'}}>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="left">Badge</TableCell>
                            <TableCell align="left">Name</TableCell>
                            <TableCell align="left">Points To Earn</TableCell>
                            <TableCell align="left">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {badges.map((badge) => {
                            const filename = getFilenameFromPath(badge.image);
                            return (
                                <TableRow
                                    key={badge.id}
                                    sx={{'&:last-child td, &:last-child th': {border: 0}}}
                                >
                                    <TableCell component="th" scope="row">
                                        {badge.id}
                                    </TableCell>
                                    <TableCell align="left"><img
                                        src={`http://localhost:8080/api/v1/auth/images/badges/${filename}`}
                                        alt={`Badge ${badge.id}`}
                                        style={{width: '50px', height: '50px'}}
                                    /></TableCell>
                                    <TableCell align="left">{badge.name}</TableCell>
                                    <TableCell align="left">{badge.pointsToEarn}</TableCell>
                                    <TableCell align="left">
                                        <IconButton onClick={() => handleOpenEditModal(badge)}>
                                            <EditIcon/>
                                        </IconButton>
                                        <IconButton onClick={() => handleOpenDeleteModal(badge)}>
                                            <DeleteIcon/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>

            <CreateBadgeModal
                isOpen={isCreateBadgeModalOpen}
                onClose={() => setCreateBadgeModalOpen(false)}
                onBadgeCreated={() => refreshBadge()} // Pass a callback function
                onCreate={(badgeData) => {
                    setCreateBadgeModalOpen(false);
                }}
            />

            <EditBadgeModal
                isOpen={isEditBadgeModalOpen}
                onClose={() => setEditBadgeModalOpen(false)}
                onBadgeEdited={() => refreshBadge()} // Add an event handler if needed
                selectedBadge={selectedBadge} // Pass the selected user data
            />

            <DeleteBadgeModal
                isOpen={isDeleteBadgeModalOpen}
                onClose={() => setDeleteBadgeModalOpen(false)}
                onBadgeDeleted={() => refreshBadge()} // Add an event handler if needed
                selectedBadge={selectedBadge} // Pass the selected user data
            />
        </div>
    );
}

export default BadgeManagement;
