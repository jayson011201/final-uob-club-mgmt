import AttendanceTable from "./AttendanceTable";
import AttendanceSummary from "./AttendanceSummary";
import React from 'react'
import AdminMenu from "../AdminMenu";

const ViewAttendance = () => {
  return (
    <div style={{marginLeft: '300px', marginRight: '80px', marginBottom: '80px', marginTop: '80px'}}>
        <AdminMenu/>
        <h1>Event Name</h1>
        <AttendanceSummary/>
        <AttendanceTable/>
    </div>
  )
}

export default ViewAttendance;