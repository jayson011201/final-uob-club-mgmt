import React, {useState, useEffect} from 'react'
import {Chart as ChartJS, BarElement, CategoryScale, LinearScale, Tooltip} from 'chart.js'
import {Bar} from 'react-chartjs-2'
import {useParams} from 'react-router-dom'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Tooltip
)

const AttendanceSummary = () => {

    const {eventId} = useParams();
    let [summary, setSummary] = useState([]);

    let countYes = summary.filter(item => item.status === 'Yes').length;
    let countNo = summary.filter(item => item.status === 'No').length;

    useEffect(() => {
        fetchData();
    }, [eventId]); // Include eventId in the dependency array

// fetch data from db based on id
    const fetchData = () => {
        fetch(`http://localhost:8080/attendance-summary/${eventId}`) //change the url based on be url
            .then((response) => response.json())
            .then((json) => {
                setSummary(json);
            })
            .catch((error) => {
                console.log(error);
            })
            .finally(() => {
            });
    };


    var data = {
        labels: ['Attending', 'Not Attending'],
        datasets: [{
            label: 'Attendance Summary',
            data: [countYes, countNo],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)'

            ],
            borderWidth: 2
        }]
    };

    var options = {
        maintainAspectRatio: true,
        beginAtZero: true,
        legend: {
            labels: {
                fontsize: 26
            }
        }
    }


    return (
        <div>
            <div className='mx-auto mt-5 '>
                {summary.length > 0 ? (
                    <Bar className='mx-20 max-h-80' data={data} options={options}></Bar>
                ) : (
                    <p>Loading...</p>
                )}

            </div>


        </div>
    )
}

export default AttendanceSummary;