import React, { useState, useEffect, useRef } from "react";
import { useParams } from "react-router-dom";
import { useReactToPrint } from "react-to-print";


function AttendanceTable() {
  const [userData, setUserData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filterStatus, setFilterStatus] = useState('');

  const componentRef = useRef();
  const {eventId} = useParams();

  useEffect(() => {
    fetchData();
  }, [eventId]); // Include eventId in the dependency array

  // fetch data from db based on id
  const fetchData = () => {
    setLoading(true); // Set loading to true when starting to fetch data
    fetch(`http://localhost:8080/view-attendance/${eventId}`)
      .then((response) => response.json())
      .then((json) => {
        setUserData(json);
      })
      .catch((error) => {
        console.log(error);
      })
      .finally(() => {
        setLoading(false); // Set loading to false when data fetching is completed
      });
  };

  //get status filter data
  const handleFilterChange = (event) => {
    setFilterStatus(event.target.value);
  };

  //render status filter
  const filteredData = userData.filter((item) => {
    if (filterStatus === '') {
      return true; // If no filter is selected, show all data
    }
    return item.status.toLowerCase() === filterStatus.toLowerCase();
  });

  // ReactToPrint hook
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
    documentTitle: 'User Data',
    onAfterPrint: () => alert('Print Success'),
  });

  function printPost() {
    return (
      <div>
        <div className="ml-4">
          <div className="flex">
            <select id="statusFilter"
              value={filterStatus}
              onChange={handleFilterChange} className=" flex-2 ml-1 border border-gray-300 rounded-full text-gray-600 h-10 pl-3 pr-5 bg-white hover:border-gray-500 focus:outline-none appearance-none cursor-pointer border-gray-900">
              <option className="hover: cursor-pointer" value=''>All</option>
              <option value="Yes">Yes</option>
              <option value="No">No</option>
            </select>
            <button onClick={handlePrint} className=" h- inline-flex items-center px-3 py-2 ml-4 bg-gray-200 hover:bg-gray-300 text-gray-800 text-sm font-medium rounded-md">
              <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-6 h-6 pr-2">
                <path strokeLinecap="round" strokeLinejoin="round" d="M6.72 13.829c-.24.03-.48.062-.72.096m.72-.096a42.415 42.415 0 0110.56 0m-10.56 0L6.34 18m10.94-4.171c.24.03.48.062.72.096m-.72-.096L17.66 18m0 0l.229 2.523a1.125 1.125 0 01-1.12 1.227H7.231c-.662 0-1.18-.568-1.12-1.227L6.34 18m11.318 0h1.091A2.25 2.25 0 0021 15.75V9.456c0-1.081-.768-2.015-1.837-2.175a48.055 48.055 0 00-1.913-.247M6.34 18H5.25A2.25 2.25 0 013 15.75V9.456c0-1.081.768-2.015 1.837-2.175a48.041 48.041 0 011.913-.247m10.5 0a48.536 48.536 0 00-10.5 0m10.5 0V3.375c0-.621-.504-1.125-1.125-1.125h-8.25c-.621 0-1.125.504-1.125 1.125v3.659M18 10.5h.008v.008H18V10.5zm-3 0h.008v.008H15V10.5z" />
              </svg>




              Print
            </button>
          </div>
        </div>

        <div ref={componentRef} className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <div className="  py-4 md:py-7 px-4 md:px-8 xl:px-10">
            <table className=" w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 ">
              <thead className="">
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    No
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Email
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Attending
                  </th>
                </tr>
              </thead>
              <tbody>
                {filteredData.map((item, index) => (
                  <tr key={index} className="border-b border-gray-200 bg-white text-sm font-semibold text-gray-700 text-center h-2">
                    <td className="px-5 py-3">{index + 1}</td>
                    <td className="px-5 py-3">{item.name}</td>
                    <td className="px-5 py-3">{item.email}</td>
                    <td className="px-5 py-3">
                      <span className={`relative inline-block px-3 py-1 font-semibold ${item.status === 'Yes' ? 'absolute inset-0 absolute inset-0 opacity-90 rounded-full bg-green-200  text-green-900' : ' absolute inset-0 absolute inset-0 opacity-90 rounded-full bg-red-200 text-red-900'} leading-tight`}>
                        <span aria-hidden></span>
                        <span className=" relative">{item.status}</span>
                      </span>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>

      </div>

    );
  }

  if (loading) {
    return <p>Loading...</p>;
  }

  return <div>{userData.length === 0 ? <p>No data available.</p> : printPost()}</div>;
}

export default AttendanceTable;
