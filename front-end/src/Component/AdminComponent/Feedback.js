import { Button } from '@mui/material';
import React from 'react'
import { useState,useEffect } from 'react';
import { Link } from 'react-router-dom';
import AdminMenu from "./AdminMenu";

const Attendance = () => {

    const [userData, setUserData] = useState([]);
  
  
  
    useEffect(() => {
      fetchData();
    }, []); // Include eventId in the dependency array
  
    // fetch data from db based on id
    const fetchData = () => {
      fetch(`http://localhost:8080/event-attendance`) //change the url based on be url
        .then((response) => response.json())
        .then((json) => {
          setUserData(json);
        })
        .catch((error) => {
          console.log(error);
        })
        .finally(() => {
        });
    };
  
  return (
    <div>
        <AdminMenu/>
        <div>
          <div className="flex">
          </div>
            <h1 className=" pl-2 mt-4" style={{fontSize: '32px', fontWeight: 'bold', marginBottom: '10px'}}>Event's Feedback List</h1>
            <div  className="relative overflow-x-auto shadow-md sm:rounded-lg mt-30" style={{marginTop: '10px'}}>

                <div className="  py-4 md:py-7 px-4 md:px-8 xl:px-10 mt-400">
            <table className=" w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400 ">
              <thead className="">
                <tr>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    No
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Event Name
                  </th>
                  <th className="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">
                    Action
                  </th>
          
                </tr>
              </thead>
              <tbody>
                {userData.map((item, index) => (
                  <tr key={index} className="border-b border-gray-200 bg-white text-sm font-semibold text-gray-700 text-center h-2">
                    <td className="px-5 py-3">
                        {index + 1}</td>
                    <td className="px-5 py-3">{item.eventname}</td>
                    <td className="px-5 py-3">
                        <Link to={`/admin/view-feedback/${item.eventid}`}>
                          <Button className="hover:outline-none  focus:outline-none" variant="outlined" color="secondary" size="small">
                          Feedback Details
                          </Button>
                        </Link ><br/>
                        
                      
                       
                    </td>
        
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Attendance;