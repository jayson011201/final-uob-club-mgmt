import React, { useState, useEffect } from 'react';
import Feedback from './Feedback'

function FeedbackManagement() {
    const [isMobileView, setIsMobileView] = useState(window.innerWidth < 600); // Adjust the threshold as needed

    useEffect(() => {
        const handleResize = () => {
            setIsMobileView(window.innerWidth < 600); // Adjust the threshold as needed
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const contentStyle = isMobileView ? {marginLeft: '16px'} : { marginLeft: '300px' };

    return (
        <div style={contentStyle}>
           <Feedback/>
        </div>
    );
}

export default FeedbackManagement;
