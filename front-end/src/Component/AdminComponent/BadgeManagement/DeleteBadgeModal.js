import React, { useState, useEffect } from 'react';
import Modal from '@mui/material/Modal';
import { Box, Button, TextField, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';

function DeleteBadgeModal({ isOpen, onClose, onBadgeDeleted, selectedBadge }) {
    const [badgeDetails, setBadgeDetails] = useState({});

    useEffect(() => {
        if (selectedBadge) {
            setBadgeDetails(selectedBadge);
        }
    }, [isOpen, selectedBadge]);

    const handleDeleteBadge = async () => {
        try {
            // Make an API call to delete the user by userId
            await axios.delete(`http://localhost:8080/api/v1/auth/delete-badge/${selectedBadge.id}`); // Uncomment this when you're ready to make the API request
            if (onBadgeDeleted) {
                onBadgeDeleted(); // Trigger the callback to refresh the table
            }
            onClose(); // Close the modal after successful deletion
        } catch (error) {
            console.error('Error deleting badge', error);
        }
    };

    return (
        <Modal open={isOpen} onClose={onClose} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <div className="modal-container" style={{ backgroundColor: 'white', padding: '20px', borderRadius: '4px', position: 'relative', maxWidth: '500px' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h2>Delete badge</h2>
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={onClose}
                        style={{ position: 'absolute', top: '10px', right: '15px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullname"
                    label="Full Name"
                    name="fullname"
                    autoComplete="name"
                    autoFocus
                    value={badgeDetails.name}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={badgeDetails.pointsToEarn}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <br />
                <br />
                <Box display="flex" justifyContent="center">
                    <Button variant="contained" color="primary" onClick={handleDeleteBadge}>
                        Delete
                    </Button>
                </Box>
            </div>
        </Modal>
    );
}

export default DeleteBadgeModal;
