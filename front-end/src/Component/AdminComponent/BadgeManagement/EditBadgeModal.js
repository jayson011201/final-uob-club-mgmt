import React, { useState, useEffect } from 'react';
import Modal from '@mui/material/Modal';
import { Box, Button, TextField, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';

function EditBadgeModal({ isOpen, onClose, onBadgeEdited, selectedBadge }) {
    const [badgeDetails, setBadgeDetails] = useState({});

    useEffect(() => {
        if (selectedBadge) {
            setBadgeDetails(selectedBadge);
        }
    }, [isOpen, selectedBadge]);

    const handleEditBadge = async () => {
        try {
            // Make an API call to delete the user by userId
            await axios.patch(`http://localhost:8080/api/v1/auth/edit-badge/${selectedBadge.id}`, badgeDetails); // Uncomment this when you're ready to make the API request
            if (onBadgeEdited) {
                onBadgeEdited(); // Trigger the callback to refresh the table
            }
            onClose(); // Close the modal after successful deletion
        } catch (error) {
            console.error('Error updating badge', error);
        }
    };

    return (
        <Modal open={isOpen} onClose={onClose} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <div className="modal-container" style={{ backgroundColor: 'white', padding: '20px', borderRadius: '4px', position: 'relative', maxWidth: '500px' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h2>Edit Badge</h2>
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={onClose}
                        style={{ position: 'absolute', top: '10px', right: '15px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="name"
                    label="Badge Name"
                    name="name"
                    autoComplete="name"
                    autoFocus
                    value={badgeDetails.name}
                    onChange={(e) => setBadgeDetails({ ...badgeDetails, name: e.target.value })}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="pointsToEarn"
                    label="Points To Earn"
                    name="pointsToEarn"
                    autoComplete="email"
                    value={badgeDetails.pointsToEarn}
                    onChange={(e) => setBadgeDetails({ ...badgeDetails, pointsToEarn: e.target.value })}
                />
                <br />
                <br />
                <Box display="flex" justifyContent="center">
                    <Button variant="contained" color="primary" onClick={handleEditBadge}>
                        Edit
                    </Button>
                </Box>
            </div>
        </Modal>
    );
}

export default EditBadgeModal;
