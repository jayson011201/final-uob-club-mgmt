import React, { useState } from 'react';
import Modal from '@mui/material/Modal';
import { Box, Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import axios from "axios";

function CreateBadgeModal({ isOpen, onClose, onBadgeCreated }) {
    const [name, setName] = React.useState('');
    const [pointsToEarn, setPointsToEarn] = React.useState('');
    const [badgeImage, setBadgeImage] = React.useState(null);

    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setOpen(false);
        setName('');
        setPointsToEarn('');
        setBadgeImage(null);
        onClose();
    };

    const handleBadgeImageChange = (e) => {
        const file = e.target.files[0];
        setBadgeImage(file);
    };

    const handleCreateBadge = async () => {
        try {
            const formData = new FormData();
            formData.append('name', name);
            formData.append('pointsToEarn', pointsToEarn);
            formData.append('badgeImage', badgeImage);

            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/new-badge',
                formData,
                {
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    },
                }
            );

            if (onBadgeCreated) {
                onBadgeCreated();
            }

            handleClose();
        } catch (error) {
            console.error('Create badge error', error);
        }
    };

    return (
        <Modal open={isOpen} onClose={onClose} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <div className="modal-container" style={{ backgroundColor: 'white', padding: '20px', borderRadius: '4px', position: 'relative' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h2>Create Badge</h2>
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={handleClose}
                        style={{ position: 'absolute', top: '10px', right: '15px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="name"
                    label="Badge Name"
                    name="name"
                    autoComplete="name"
                    autoFocus
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="pointsToEarn"
                    label="Points To Earn"
                    name="pointsToEarn"
                    autoComplete="pointsToEarn"
                    value={pointsToEarn}
                    onChange={(e) => setPointsToEarn(e.target.value)}
                />
                <input
                    accept="image/*"
                    style={{ display: 'none' }}
                    id="badge-image"
                    type="file"
                    onChange={handleBadgeImageChange}
                />
                <label htmlFor="badge-image">
                    <Button variant="contained" component="span">
                        Upload Badge Image
                    </Button>
                </label>
                {badgeImage && <p>{badgeImage.name}</p>}
                <br />
                <br />
                <Box display="flex" justifyContent="center">
                    <Button variant="contained" color="primary" onClick={handleCreateBadge}>
                        Create
                    </Button>
                </Box>
            </div>
        </Modal>
    );
}

export default CreateBadgeModal;
