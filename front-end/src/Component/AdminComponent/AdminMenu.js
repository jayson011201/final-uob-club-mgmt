import * as React from 'react';
import {useEffect, useState} from 'react';
import { Link } from 'react-router-dom'
import {
    Drawer, List, ListItem, ListItemIcon, ListItemText, Box, Divider, IconButton, useMediaQuery, Avatar, Button
} from '@mui/material';
import { styled, useTheme } from '@mui/material/styles';
import PeopleIcon from '@mui/icons-material/People';
import LogoutIcon from '@mui/icons-material/Logout';
import MenuIcon from '@mui/icons-material/Menu';
import EventIcon from '@mui/icons-material/Event';
import HowToRegIcon from '@mui/icons-material/HowToReg';
import FeedbackIcon from '@mui/icons-material/Feedback';
import HomeIcon from '@mui/icons-material/Home';
import StarIcon from '@mui/icons-material/Star';
import BadgeIcon from '@mui/icons-material/Badge';
import '../../CSS/AdminMenu.css';
import axios from "axios";

const defaultProfileImage = "default.jfif";
//const drawerWidth = 240;

const StyledDrawer = styled(Drawer)(({ theme }) => ({
    width: '100%',
    maxWidth: '240px',
    flexShrink: 0,
    '& .MuiDrawer-paper': {
        width: '100%',
        maxWidth: '240px',
        boxSizing: 'border-box',
    },
}));

const CustomLink = React.forwardRef(({ to, children }, ref) => (
    <Link ref={ref} to={to} style={{
        textDecoration: 'none',
        color: 'inherit',
        display: 'flex',
        paddingTop: '10px',
        paddingBottom: '10px',
        paddingLeft: '15px',
    }}>
        {children}
    </Link>
));


function AdminMenu() {
    const theme = useTheme();
    const [email, setEmail] = useState(null);
    const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
    const [mobileOpen, setMobileOpen] = useState(false);
    const [profileImagePath, setProfileImagePath] = useState(null);

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const getFilenameFromPath = (filePath) => {
        const parts = filePath.split(/\\+/);
        return parts[parts.length - 1];
    };

    useEffect(() => {
        const fetchProfileImagePath = async () => {
            try {
                const userId = localStorage.getItem('id');
                setEmail(localStorage.getItem('email'));
                const userPicture = await axios.get(`http://localhost:8080/api/v1/auth/users/${userId}`);
                const filePath = userPicture.data.profile;

                const profileImagePath = filePath ? getFilenameFromPath(filePath) : defaultProfileImage;

                setProfileImagePath(profileImagePath);
            } catch (error) {
                console.error('Error fetching profile image:', error);
                setProfileImagePath(defaultProfileImage);
            }
        };

        fetchProfileImagePath();
    }, []);

    const handleLogout = () => {
        localStorage.clear();
        setTimeout(() => {
            window.location.href = '/login';
        }, 1000);
    };

    const handleToHome = () => {
        setTimeout(() => {
            window.location.href = '/admin/home';
        }, 1000);
    };

    const drawer = (
        <Box sx={{ overflow: 'auto', display: 'flex', flexDirection: 'column', height: '100%' }}>
            <Box sx={{ p: 2, display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
                <Button href={'/admin/profile'}>
                <img
                    src={`http://localhost:8080/api/v1/auth/images/${profileImagePath}`}
                    alt="Profile"
                    className="profile-image"
                    style={{
                        width: '65px',
                        height: '65px',
                        objectFit: 'cover',
                        borderRadius: '50%',
                    }}
                    onError={() => setProfileImagePath(defaultProfileImage)}
                />
                </Button>
                <ListItemText primary={email} />
            </Box>
            <Divider />
            <List>
                <ListItem button component={CustomLink} to={'/admin/user'}>
                    <ListItemIcon>
                        <PeopleIcon />
                    </ListItemIcon>
                        <ListItemText primary={'User'}/>
                </ListItem>
                <ListItem button component={CustomLink} to={'/admin/event'}>
                    <ListItemIcon>
                        <EventIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Event'} />
                </ListItem>
                <ListItem button component={CustomLink} to={'/admin/attendance'}>
                    <ListItemIcon>
                        <HowToRegIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Attendance'} />
                </ListItem>
                <ListItem button component={CustomLink} to={'/admin/feedback'}>
                    <ListItemIcon>
                        <FeedbackIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Feedback'} />
                </ListItem>
                <ListItem button component={CustomLink} to={'/admin/point'}>
                    <ListItemIcon>
                        <StarIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Point'} />
                </ListItem>
                <ListItem button component={CustomLink} to={'/admin/badge'}>
                    <ListItemIcon>
                        <BadgeIcon />
                    </ListItemIcon>
                    <ListItemText primary={'Badge'} />
                </ListItem>
            </List>
            <Box sx={{ mt: 'auto', mb: 2 }}>
                <List>
                    <ListItem onClick={handleToHome} button>
                        <ListItemIcon>
                            <HomeIcon />
                        </ListItemIcon>
                        <ListItemText primary="Home" />
                    </ListItem>
                    <ListItem button onClick={handleLogout}>
                        <ListItemIcon>
                            <LogoutIcon />
                        </ListItemIcon>
                        <ListItemText primary="Log out" />
                    </ListItem>
                </List>
            </Box>
        </Box>
    );

    return (
        <Box>
            {isMobile && (
                <IconButton
                    color="inherit"
                    aria-label="open drawer"
                    edge="start"
                    onClick={handleDrawerToggle}
                    sx={{ marginLeft: '8px', marginTop: '8px' }}
                >
                    <MenuIcon />
                </IconButton>
            )}
            <StyledDrawer
                variant={isMobile ? 'temporary' : 'permanent'}
                open={mobileOpen}
                onClose={handleDrawerToggle}
                anchor="left"
            >
                {drawer}
            </StyledDrawer>
        </Box>
    );
}

export default AdminMenu;
