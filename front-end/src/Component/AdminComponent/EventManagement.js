import React, { useState, useEffect } from 'react';
import Mission from '../HomeComponent/Mission';
import CreateEvent from './EventComponent/CreateEvent';
import PanelEvent from './EventComponent/PanelEvent';
import EditEvent from './EventComponent/EditEvent';
import DetailsEvent from './EventComponent/DetailsEvent';
import AdminMenu from "./AdminMenu";



function EventManagement() {
    const [isMobileView, setIsMobileView] = useState(window.innerWidth < 600); // Adjust the threshold as needed

    useEffect(() => {
        const handleResize = () => {
            setIsMobileView(window.innerWidth < 600); // Adjust the threshold as needed
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const contentStyle = isMobileView ? {marginLeft: '16px'} : { marginLeft: '300px' };

    return (
        <div style={contentStyle}>
            <AdminMenu/>
            <h1 style={{fontSize: '35px', fontWeight: 'bold', paddingTop: '30px'}}>
                Event Management
            </h1>
            <CreateEvent></CreateEvent>
          <PanelEvent></PanelEvent>
        
        </div>
    );
}

export default EventManagement;
