import React, { useState, useEffect } from 'react';
import Modal from '@mui/material/Modal';
import { Box, Button, TextField, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';

function UserDetailModal({ isOpen, onClose, selectedUser }) {
    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {
        if (selectedUser) {
            setUserDetails(selectedUser); // Set userDetails only if selectedUser is defined
            if (isOpen) {
                // Fetch user details by userId when the modal is opened
                // fetchUserDetails(selectedUser.id); // Uncomment this when you're ready to make the API request
            }
        }
    }, [isOpen, selectedUser]);

    return (
        <Modal open={isOpen} onClose={onClose} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <div className="modal-container" style={{ backgroundColor: 'white', padding: '20px', borderRadius: '4px', position: 'relative', maxWidth: '500px' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h2>User Details</h2>
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={onClose}
                        style={{ position: 'absolute', top: '10px', right: '15px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullname"
                    label="Full Name"
                    name="fullname"
                    autoComplete="name"
                    autoFocus
                    value={userDetails.fullname}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={userDetails.email}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="phone"
                    label="Phone Number"
                    name="phone"
                    autoComplete="tel"
                    value={userDetails.phone}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="gender"
                    label="Gender"
                    id="gender"
                    value={userDetails.gender}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <br />
                <br />
                <Box display="flex" justifyContent="center">
                    <Button variant="contained" color="primary" onClick={onClose}>
                        Close
                    </Button>
                </Box>
            </div>
        </Modal>
    );
}

export default UserDetailModal;
