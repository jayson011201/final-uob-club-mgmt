import React, { useState } from 'react';
import Modal from '@mui/material/Modal';
import {Box, Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField, IconButton} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import axios from "axios";

function CreateUserModal({ isOpen, onClose, onUserCreated }) {
    const [gender, setGender] = React.useState('');
    const [fullname, setFullname] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [activationCode, setActivationCode] = useState('');

    const [open, setOpen] = useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => {
        setOpen(false);
        setGender('');
        setFullname('');
        setEmail('');
        setPhone('');
        setPassword('');
        setConfirmPassword('');
        onClose();
    };

    const handleCreateUser = async () => {
        try {
            const registrationData = {
                fullname,
                email,
                phone,
                gender,
                password,
                confirmPassword,
            };

            console.log(registrationData);

            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/signup',
                registrationData
            );

            if (onUserCreated){
                onUserCreated();
            }

            console.log('Registration successful', response.data);

            handleClose();
        } catch (error) {
            console.error('Registration error', error);
        }
    };

    const handleActivation = async () => {
        try {
            // Make an API call to check the verification code
            const response = await axios.post(
                'http://localhost:8080/api/v1/auth/verify-otp',
                { code: activationCode }
            );

            if (response.status === 200) {
                console.log('Activation Successful');
            } else {
                // Code is invalid, handle the error case here
                console.error('Invalid activation code');
            }

            // Close the modal
            handleClose();
        } catch (error) {
            console.error('Activation error', error);
        }
    };

    const handleGenderChange = (event) => {
        setGender(event.target.value);
    };

    return (
        <Modal open={isOpen} onClose={onClose} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <div className="modal-container" style={{ backgroundColor: 'white', padding: '20px', borderRadius: '4px', position: 'relative' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h2>Create User</h2>
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={handleClose}
                        style={{ position: 'absolute', top: '10px', right: '15px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullname"
                    label="Full Name"
                    name="fullname"
                    autoComplete="name"
                    autoFocus
                    value={fullname}
                    onChange={(e) => setFullname(e.target.value)}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <Grid container spacing={2}>
                    <Grid item xs={12} sm={7}>
                        <TextField
                            margin="normal"
                            required
                            fullWidth
                            id="phone"
                            label="Phone Number"
                            name="phone"
                            autoComplete="tel"
                            value={phone}
                            onChange={(e) => setPhone(e.target.value)}
                        />
                    </Grid>
                    <Grid item xs={12} sm={5}>
                        <FormControl fullWidth margin="normal">
                            <InputLabel id="gender-label">Gender</InputLabel>
                            <Select
                                labelId="gender-label"
                                id="gender"
                                value={gender}
                                label="Gender"
                                onChange={handleGenderChange}
                            >
                                <MenuItem value={'Female'}>Female</MenuItem>
                                <MenuItem value={'Male'}>Male</MenuItem>
                                <MenuItem value={'Other'}>Other</MenuItem>
                            </Select>
                        </FormControl>
                    </Grid>
                </Grid>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="new-password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="confirmPassword"
                    label="Confirm Password"
                    type="password"
                    id="confirmPassword"
                    autoComplete="new-password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                />
                <br/>
                <br/>
                <Box display="flex" justifyContent="center">
                    <Button variant="contained" color="primary" onClick={handleCreateUser}>
                        Create
                    </Button>
                </Box>
            </div>
        </Modal>
    );
}

export default CreateUserModal;
