import React, { useState, useEffect } from 'react';
import Modal from '@mui/material/Modal';
import {Box, Button, FormControl, TextField, IconButton, InputLabel, Select, MenuItem} from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import axios from 'axios';

function EditUserModal({ isOpen, onClose, onUserEdited, selectedUser }) {
    const [userDetails, setUserDetails] = useState({});
    const [gender, setGender] = useState('');

    const handleGenderChange = (event) => {
        setGender(event.target.value);
    };

    useEffect(() => {
        if (selectedUser) {
            setUserDetails(selectedUser);
            setGender(selectedUser.gender);
            if (isOpen) {
                // Fetch user details by userId when the modal is opened
                // fetchUserDetails(selectedUser.id); // Uncomment this when you're ready to make the API request
            }
        }
    }, [isOpen, selectedUser]);

    const handleEditUser = async () => {
        try {
            // Make an API call to delete the user by userId
            await axios.patch(`http://localhost:8080/api/v1/auth/users/${selectedUser.id}`, userDetails); // Uncomment this when you're ready to make the API request
            if (onUserEdited) {
                onUserEdited(); // Trigger the callback to refresh the table
            }
            onClose(); // Close the modal after successful deletion
        } catch (error) {
            console.error('Error updating user', error);
        }
    };

    return (
        <Modal open={isOpen} onClose={onClose} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
            <div className="modal-container" style={{ backgroundColor: 'white', padding: '20px', borderRadius: '4px', position: 'relative', maxWidth: '500px' }}>
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <h2>Edit User</h2>
                    <IconButton
                        edge="end"
                        color="inherit"
                        onClick={onClose}
                        style={{ position: 'absolute', top: '10px', right: '15px' }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="fullname"
                    label="Full Name"
                    name="fullname"
                    autoComplete="name"
                    autoFocus
                    value={userDetails.fullname}
                    onChange={(e) => setUserDetails({ ...userDetails, fullname: e.target.value })}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    value={userDetails.email}
                    InputProps={{
                        readOnly: true, // Make the field readonly
                    }}
                />
                <TextField
                    margin="normal"
                    required
                    fullWidth
                    id="phone"
                    label="Phone Number"
                    name="phone"
                    autoComplete="tel"
                    value={userDetails.phone}
                    onChange={(e) => setUserDetails({ ...userDetails, phone: e.target.value })}
                />
                <FormControl fullWidth margin="normal">
                    <InputLabel id="gender-label">Gender</InputLabel>
                    <Select
                        labelId="gender-label"
                        id="gender"
                        value={gender} // Set the value to userDetails.gender
                        label="Gender"
                        onChange={handleGenderChange}
                    >
                        <MenuItem value={'Female'}>Female</MenuItem>
                        <MenuItem value={'Male'}>Male</MenuItem>
                        <MenuItem value={'Other'}>Other</MenuItem>
                    </Select>
                </FormControl>
                <br />
                <br />
                <Box display="flex" justifyContent="center">
                    <Button variant="contained" color="primary" onClick={handleEditUser}>
                        Edit
                    </Button>
                </Box>
            </div>
        </Modal>
    );
}

export default EditUserModal;
