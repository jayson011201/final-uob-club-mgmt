import React, { useState, useEffect } from 'react';
import AttendanceForm from '../UserComponent/AttendanceForm';
import AttendanceSummary from './AttendanceManagements/AttendanceSummary';
import AttendanceTable from './AttendanceManagements/AttendanceTable';
import Attendance from './Attendance';

function AttendanceManagement() {
    const [isMobileView, setIsMobileView] = useState(window.innerWidth < 600); // Adjust the threshold as needed

    useEffect(() => {
        const handleResize = () => {
            setIsMobileView(window.innerWidth < 600); // Adjust the threshold as needed
        };

        window.addEventListener('resize', handleResize);

        return () => {
            window.removeEventListener('resize', handleResize);
        };
    }, []);

    const contentStyle = isMobileView ? {marginLeft: '16px'} : { marginLeft: '300px', marginRight: '80px', marginTop: '30px' };

    return (
        <div style={contentStyle}>
               <h1 style={{fontSize: '32px', fontWeight: 'bold', marginBottom: '10px'}}>
                Attendance Summary
            </h1>
            {/* <div className='mt-5'>
            <AttendanceSummary/>
            </div>
            <div>
            <AttendanceTable/>
            </div> */}

            <Attendance/>
           
        </div>
    );
}

export default AttendanceManagement;
