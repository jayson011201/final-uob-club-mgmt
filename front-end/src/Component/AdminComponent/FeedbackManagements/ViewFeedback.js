import React from 'react'
import FeedbackTable from "./FeedbackTable"
import FeedbackSummary from './FeedbackSummary'
import AdminMenu from "../AdminMenu";

const ViewFeedback = () => {
  return (
    <div style={{marginLeft: '300px', marginRight: '80px', marginBottom: '30px'}}>
        <AdminMenu/>
      <div className='mx-auto items-center mt-6'>
      <FeedbackSummary/>
      </div>
      <div>
           <FeedbackTable/>
      </div>
    </div>
  )
}

export default ViewFeedback