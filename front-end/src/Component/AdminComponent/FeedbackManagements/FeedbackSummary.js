import React, { useState, useEffect } from 'react'
import { Chart as ChartJS, BarElement, CategoryScale, LinearScale, Tooltip, Legend } from 'chart.js'
import { Bar } from 'react-chartjs-2'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Tooltip,
    Legend
)

const FeedbackSummary = () => {
    let [summary, setSummary]=useState([]);

    //event ID
    let eventId =1;
   
    //  Question 1
      let countQ1S = summary.filter(item => item.answer1 === 'Satisfied').length;
      let countQ1N = summary.filter(item => item.answer1 === 'Neutral').length;
      let countQ1D = summary.filter(item => item.answer1 === 'Dissatisfied').length;
      //Question 2
      let countQ2S = summary.filter(item => item.answer2 === 'Satisfied').length;
      let countQ2N = summary.filter(item => item.answer2 === 'Neutral').length;
      let countQ2D = summary.filter(item => item.answer2 === 'Dissatisfied').length;
      //Question 3
      let countQ3S = summary.filter(item => item.answer3 === 'Satisfied').length;
      let countQ3N = summary.filter(item => item.answer3 === 'Neutral').length;
      let countQ3D = summary.filter(item => item.answer3 === 'Dissatisfied').length;
      //Question 4
      let countQ4S = summary.filter(item => item.answer4 === 'Satisfied').length;
      let countQ4N = summary.filter(item => item.answer4 === 'Neutral').length;
      let countQ4D = summary.filter(item => item.answer4 === 'Dissatisfied').length;
     

    useEffect(() => {
        fetchData();

   
      }, [eventId]); // Include eventId in the dependency array
    
      // fetch data from db based on id
      const fetchData = () => {
        fetch(`http://localhost:8080/feedback-summary/${eventId}`) //change the url based on be url
          .then((response) => response.json())
          .then((json) => {
            setSummary(json);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => {
          });

          
      };
          
    const data = {
        
        labels: ['Overall Event', 'Food', `Event's Activities`, `Event's Management`],
        datasets: [
          {
            label: 'Satisfied',
            data: [countQ1S, countQ2S, countQ3S, countQ4S],
            backgroundColor: 'rgba(75, 192, 192, 0.2)',
            borderColor:'rgb(75, 192, 192)',
            borderWidth: 1
          },
          {
            label: 'Neutral',
            data: [countQ1N, countQ2N, countQ3N, countQ4N],
            backgroundColor: 'rgba(201, 203, 207, 0.2)',
            borderColor:'rgb(220, 223, 227)',
            borderWidth: 1
          },
          {
            label: 'Dissatisfied',
            data: [countQ1D, countQ2D, countQ3D, countQ4D],
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor:'rgb(255, 99, 132)',
            borderWidth: 1
          },
        ],
      };

    const options = {
      maintainAspectRatio: true,
      beginAtZero:true,
      legend:{
          labels:{
              fontsize:26
          }
        }

    }

    return (
        <div>
             {summary.length > 0 ? (
            <Bar className=' mx-20  max-h-80' data={data} options={options}></Bar>
        ) : (
            <p>Loading...</p>
        )}
        </div>
    )
}

export default FeedbackSummary