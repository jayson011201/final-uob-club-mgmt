import React, { useState, useEffect } from 'react';
import {
    Paper,
    TableContainer,
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    TablePagination,
    TextField,
    IconButton,
    TableSortLabel,
} from "@mui/material";
import axios from 'axios';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import {height} from "@mui/system";
import AdminMenu from "./AdminMenu";

function PointManagement() {
    const [isMobileView, setIsMobileView] = useState(window.innerWidth < 600);
    const [points, setPoints] = useState([]);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(10);
    const [searchTerm, setSearchTerm] = useState('');
    const [sortOrder, setSortOrder] = useState('asc');
    const [sortBy, setSortBy] = useState('id');

    const refreshPoint = () => {
        fetchPoints();
    };

    useEffect(() => {
        fetchPoints();
    }, [page, rowsPerPage, searchTerm, sortOrder, sortBy]);

    const fetchPoints = async () => {
        try {
            const response = await axios.get(`http://localhost:8080/api/v1/auth/points?page=${page}&pageSize=${rowsPerPage}`);
            let filteredPoints = response.data;

            // Search
            if (searchTerm) {
                filteredPoints = filteredPoints.filter(point =>
                    point.email.toLowerCase().includes(searchTerm.toLowerCase()) ||
                    point.pointAmount.toString().toLowerCase().includes(searchTerm.toLowerCase()) ||
                    point.pointEarnFrom.toLowerCase().includes(searchTerm.toLowerCase()) ||
                    point.pointEarnDate.toLowerCase().includes(searchTerm.toLowerCase())
                );
            }

            // Sort
            filteredPoints.sort((a, b) => {
                const comparison = sortOrder === 'asc' ? 1 : -1;
                return a[sortBy] > b[sortBy] ? comparison : -comparison;
            });

            setPoints(filteredPoints);
        } catch (error) {
            console.log('Error fetching points data', error);
        }
    };

    const handleSearchChange = (event) => {
        setSearchTerm(event.target.value);
    };

    const handleSortChange = (column) => {
        if (column === sortBy) {
            setSortOrder((prevSortOrder) => (prevSortOrder === 'desc' ? 'asc' : 'desc'));
        } else {
            setSortBy(column);
            setSortOrder('asc');
        }
    };

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const contentStyle = isMobileView ? { marginLeft: '16px', marginRight: '16px' } : { marginLeft: '300px', marginRight: '80px', marginTop: '30px'  };

    return (
        <div style={contentStyle}>
            <AdminMenu/>
            <h1 style={{fontSize: '35px', fontWeight: 'bold', marginBlock: '10px'}}>Points Log</h1>
            <TextField
                label="Search"
                variant="outlined"
                onChange={handleSearchChange}
            />
            <br />
            <br />
            <TableContainer component={Paper}>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={100} // Replace with the actual count or get it from the server
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
                <Table sx={{ minWidth: 650, overflowX: 'auto' }} aria-label="simple table">
                    <TableHead sx={{ backgroundColor: '#eeeeee' }}>
                        <TableRow>
                            <TableCell>
                                <TableSortLabel
                                    active={sortBy === 'id'}
                                    direction={sortOrder}
                                    onClick={() => handleSortChange('id')}>
                                    ID
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>
                                <TableSortLabel
                                    active={sortBy === 'email'}
                                    direction={sortOrder}
                                    onClick={() => handleSortChange('email')}>
                                    Email
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>
                                <TableSortLabel
                                    active={sortBy === 'pointAmount'}
                                    direction={sortOrder}
                                    onClick={() => handleSortChange('pointAmount')}>
                                    Points Earn
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>
                                <TableSortLabel
                                    active={sortBy === 'pointEarnFrom'}
                                    direction={sortOrder}
                                    onClick={() => handleSortChange('pointEarnFrom')}>
                                    Earn From
                                </TableSortLabel>
                            </TableCell>
                            <TableCell>
                                <TableSortLabel
                                    active={sortBy === 'pointEarnDate'}
                                    direction={sortOrder}
                                    onClick={() => handleSortChange('pointEarnDate')}>
                                    Date Time
                                </TableSortLabel>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {points.map((point) => (
                            <TableRow
                                key={point.id}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                                <TableCell component="th" scope="row">
                                    {point.id}
                                </TableCell>
                                <TableCell align="left">{point.email}</TableCell>
                                <TableCell align="left">{point.pointAmount}</TableCell>
                                <TableCell align="left">{point.pointEarnFrom}</TableCell>
                                <TableCell align="left">{point.pointEarnDate}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}

export default PointManagement;
