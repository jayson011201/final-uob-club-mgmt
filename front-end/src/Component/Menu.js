import React, { useState, useEffect } from 'react';
import { AppBar, Toolbar, Button, Box, Alert, Snackbar } from '@mui/material';
import Logo from '../Asset/logo.jpg';
import { Link as ScrollLink } from 'react-scroll';
import ScrollToBottom from "./ScrollToBottom";
import { useLocation, useNavigate } from 'react-router-dom';
import axios from 'axios';

const defaultProfileImage = "default.jfif";

const isAuthenticated = () => {
    const token = localStorage.getItem('token');
    return !!token;
};

const isAdmin = () => {
    const role = localStorage.getItem('role');
    return role === "ADMIN";
};

const getFilenameFromPath = (filePath) => {
    const parts = filePath.split(/\\+/);
    return parts[parts.length - 1];
};

function Menu() {
    const location = useLocation();
    const [isSuccessAlertOpen, setIsSuccessAlertOpen] = useState(false);
    const navigate = useNavigate();
    const [profileImagePath, setProfileImagePath] = useState(null);

    const handleLeaderboardClick = () => {
        if (isAdmin()) {
            // Navigate to the admin leaderboard page
            navigate('/admin/leaderboard');
        } else {
            // Navigate to the user leaderboard page
            navigate('/member/leaderboard');
        }
    };

    const handleLogout = () => {
        localStorage.clear();
        setIsSuccessAlertOpen(true);
        setTimeout(() => {
            window.location.href = '/login';
        }, 1000);
    };

    const goToHome = () => {
        const userRole = localStorage.getItem('role');
        // Determine the redirect path based on the user's role
        const homePath = userRole === 'ADMIN' ? '/admin/home' : '/member/home';
        navigate(homePath);
    };

    useEffect(() => {
        const fetchProfileImagePath = async () => {
            try {
                const userId = localStorage.getItem('id');
                const userPicture = await axios.get(`http://localhost:8080/api/v1/auth/users/${userId}`);
                const filePath = userPicture.data.profile;

                const profileImagePath = filePath ? getFilenameFromPath(filePath) : defaultProfileImage;

                setProfileImagePath(profileImagePath);
            } catch (error) {
                console.error('Error fetching profile image:', error);
                setProfileImagePath(defaultProfileImage);
            }
        };

        fetchProfileImagePath();
    }, []);

    const renderButtons = () => {
        if (isAuthenticated()) {
            if (
                location.pathname === '/member/leaderboard' ||
                location.pathname.startsWith('/member/list-event/') ||
                location.pathname.startsWith('/member/register/') ||
                location.pathname === '/member/profile' ||
                location.pathname === '/admin/leaderboard' ||
                location.pathname.startsWith('/admin/list-event/') ||
                location.pathname.startsWith('/admin/register/') ||
                location.pathname === '/admin/profile'
            ) {
                return (
                    <>
                        <Button color="inherit" sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }} onClick={goToHome}>
                            Back to Home
                        </Button>
                    </>
                );
            } else {
                return (
                    <>
                        <Button color="inherit" sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }}>
                            <ScrollLink to="home-section" smooth={true} duration={500} offset={-80}>Home</ScrollLink>
                        </Button>
                        <Button color="inherit" sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }}>
                            <ScrollLink to="event-section" smooth={true} duration={500} offset={-80}>Events</ScrollLink>
                        </Button>
                        <Button color="inherit" sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }}>
                            <ScrollLink to="faq-section" smooth={true} duration={500} offset={-80}>FAQs</ScrollLink>
                        </Button>
                        <Button color="inherit" sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }} onClick={ScrollToBottom}>
                            Contact Us
                        </Button>
                        {isAuthenticated() && (
                            <Button
                                color="inherit"
                                sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }}
                                onClick={handleLeaderboardClick}
                            >
                                Leaderboard
                            </Button>
                        )}
                    </>
                );
            }
        } else {
            return null;
        }
    };

    return (
        <AppBar position="fixed" sx={{ backgroundColor: '#ffffff', zIndex: (theme) => theme.zIndex.drawer + 1 }}>
            <Toolbar sx={{ minHeight: '80px' }}>
                <Box sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}>
                    <img src={Logo} alt="Logo" style={{ maxHeight: '75px', margin: '5px 0 0 0' }} />
                </Box>
                <Box sx={{ flexGrow: 1, display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                    {renderButtons()}
                </Box>
                <Box sx={{
                    flexGrow: 1,
                    display: 'flex',
                    justifyContent: 'flex-end',
                }}>
                    {isAdmin() && (
                        <Button
                            color="inherit"
                            sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }}
                            href={'/admin/user'}
                        >
                            Admin
                        </Button>
                    )}
                    <Button
                        color="inherit"
                        sx={{ fontSize: '0.9rem', margin: '0 10px', color: '#333' }}
                        onClick={isAuthenticated() ? handleLogout : () => navigate('/login')}
                    >
                        {isAuthenticated() ? 'Logout' : 'Login'}
                    </Button>
                    {isAuthenticated() && (
                        <Button
                            color="inherit"
                            sx={{
                                fontSize: '0.9rem',
                                margin: '0 10px',
                                color: '#333',
                                width: '64px',
                                height: '64px',
                                borderRadius: '50%',
                                padding: '0',
                                overflow: 'hidden',
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                            href={isAdmin() ? '/admin/profile' : '/member/profile'}
                        >
                            <img
                                src={`http://localhost:8080/api/v1/auth/images/${profileImagePath}`}
                                alt="Profile"
                                className="profile-image"
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    objectFit: 'cover',
                                    borderRadius: '50%',
                                }}
                                onError={() => setProfileImagePath(defaultProfileImage)}
                            />
                        </Button>
                    )}
                </Box>
            </Toolbar>

            <Snackbar
                open={isSuccessAlertOpen}
                autoHideDuration={1000}
                onClose={() => setIsSuccessAlertOpen(false)}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                elevation={3}
            >
                <Alert onClose={() => setIsSuccessAlertOpen(false)} severity="error">
                    Logout from your account. Loading...
                </Alert>
            </Snackbar>
        </AppBar>
    );
}

export default Menu;
