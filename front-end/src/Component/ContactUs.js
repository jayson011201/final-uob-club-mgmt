import React from 'react';
import { Box, Typography, Container, Grid, Paper } from '@mui/material';
import { styled } from '@mui/system';
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import YouTubeIcon from '@mui/icons-material/YouTube';
import EmailIcon from '@mui/icons-material/Email';

const ContactUsFooter = styled('footer')({
    backgroundColor: '#333',
    color: '#fff',
    padding: '20px',
    width: '100%',
    bottom: 0,
    textAlign: 'center', // Center the content
});

const ContactDetails = styled('div')({
    flex: '1',
    marginRight: '20px',
});

const ContactList = styled('ul')({
    listStyle: 'none',
    padding: '0',
});

const ContactListItem = styled('li')({
    fontSize: '14px',
    marginBottom: '8px',
});

const SocialMediaIcons = styled('div')({
    display: 'flex',
    justifyContent: 'center',
    marginTop: '20px', // Adjust as needed
});

const SocialMediaIcon = styled('div')({
    fontSize: '24px',
    margin: '0 10px',
    cursor: 'pointer',
});

function ContactUs() {
    return (
        <ContactUsFooter>
            <Container>
                <Grid container>
                    <Grid item xs={12} md={12}>
                        <ContactDetails>
                            <Typography variant="h4">Contact Us</Typography>
                            <Typography variant="body1">
                                Have questions or need assistance? Feel free to reach out to us.
                            </Typography>
                                <ContactListItem>Email: 1nity@gamil.com</ContactListItem>
                                <ContactListItem>Phone: 60+123456789</ContactListItem>
                        </ContactDetails>
                    </Grid>
                    <Grid item xs={12} md={12}>
                        <Paper className="contact-form">
                            {/* Include a contact form or any other contact options here */}
                        </Paper>
                        <SocialMediaIcons>
                            <SocialMediaIcon>
                                <FacebookIcon />
                            </SocialMediaIcon>
                            <SocialMediaIcon>
                                <TwitterIcon />
                            </SocialMediaIcon>
                            <SocialMediaIcon>
                                <InstagramIcon />
                            </SocialMediaIcon>
                            <SocialMediaIcon>
                                <YouTubeIcon />
                            </SocialMediaIcon>
                            <SocialMediaIcon>
                                <EmailIcon />
                            </SocialMediaIcon>
                        </SocialMediaIcons>
                    </Grid>
                </Grid>
            </Container>
        </ContactUsFooter>
    );
}

export default ContactUs;
