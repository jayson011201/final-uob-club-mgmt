import React, {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import axios from 'axios';
import {Typography, Paper, Grid, Button, Snackbar} from '@mui/material';

function RegisterEvent() {
    const {id} = useParams();
    const eventid = id;
    const [userid, setUserid] = useState(null);
    const [eventDetails, setEventDetails] = useState(null);
    const [isSnackbarOpen, setIsSnackbarOpen] = useState(false);

    useEffect(() => {
        setUserid(localStorage.getItem('id'));
        const fetchEventDetails = async () => {
            try {
                const response = await axios.get(`http://localhost:8080/events/${eventid}`);
                setEventDetails(response.data.body);
            } catch (error) {
                console.error('Error fetching event details:', error);
            }
        };

        fetchEventDetails();
    }, [eventid]);

    const handleRegister = async () => {
        try {
            const registrationDate = new Date().toISOString(); // Capture the current date/time

            const response = await axios.post(`http://localhost:8080/register/${userid}/${eventid}`, {
                registerdate: registrationDate
            });
            setIsSnackbarOpen(true);
        } catch (error) {
            console.error('Error registering for the event:', error);
            // Handle error and provide feedback to the user
        }
    };

    const handleCloseSnackbar = () => {
        setIsSnackbarOpen(false);
    };

    if (!eventDetails) {
        return <div>Loading...</div>;
    }

    return (
        <div style={{padding: '20px'}}>
            <Typography variant="h2" gutterBottom>
                Event Details
            </Typography>

            <Paper elevation={3} style={{padding: '20px'}}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Typography variant="h4">{eventDetails.eventname}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1">Event Description: {eventDetails.eventdesc}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1">Event Date: {eventDetails.eventdate}</Typography>
                    </Grid>
                    <Grid item xs={12}>
                        <Typography variant="body1">Event Time: {eventDetails.eventtime}</Typography>
                    </Grid>
                    {/* Add more details as needed */}
                    <Grid item xs={12}>
                        <Button variant="contained" color="primary" onClick={handleRegister}>
                            Register for Event
                        </Button>
                    </Grid>
                </Grid>
            </Paper>


            <Snackbar
                open={isSnackbarOpen}
                autoHideDuration={6000} // Adjust the duration as needed
                onClose={handleCloseSnackbar}
                message="Successfully registered for the event!"
            />
        </div>
    );

}

export default RegisterEvent;

