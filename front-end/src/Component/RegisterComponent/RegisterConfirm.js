function RegisterConfirm() {
    // members will see a pop up after clicking register on the RegisterEvent page. the pop up
    // will require the member to confirm their email address for register confirmation. 
    // they will then receive the email that they have successfully registered
}
export default RegisterConfirm;