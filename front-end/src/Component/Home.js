import Slideshow from "./HomeComponent/Slideshow";
import Faq from "./HomeComponent/Faq";
import ContactUs from "./ContactUs";
import Slogan from "./HomeComponent/Slogan";
import Mission from "./HomeComponent/Mission";
import Event from "./HomeComponent/Event";
import Menu from "./Menu";

function Home() {
    return(
        // <div className="container" style={{paddingTop: '80px', marginLeft: '10%', marginRight: '10%'}}>
        <div>
            <Menu></Menu>
            <div id="home-section">
                <Slideshow/>
            </div>
            <div id="slogan-section">
                <Slogan />
            </div>
            <div id="mission-section">
                <Mission />
            </div>
            <div id="event-section">
                <Event />
            </div>
            <div id="faq-section">
                <Faq/>
            </div>
            <ContactUs></ContactUs>
        </div>
    )
}

export default Home;