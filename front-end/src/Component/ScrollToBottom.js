import React from "react";

    const ScrollBottom = () => {
        window.scrollTo({
            top: document.documentElement.scrollHeight,
            behavior: 'smooth', // This enables smooth scrolling
        });
    };

export default ScrollBottom;