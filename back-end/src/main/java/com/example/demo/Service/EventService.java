package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class EventService {
    @Value("${event.file.path}")
    private String imagePath;

    public String handleFileUpload(MultipartFile file, Long userId) {
        try {
            String uploadDirectory = imagePath;

            String originalFilename = file.getOriginalFilename();

            byte[] bytes = file.getBytes();
            Path filePath = Paths.get(uploadDirectory, originalFilename);
            Files.write(filePath, bytes);

            return "File uploaded successfully! " + filePath;
        } catch (Exception e) {
            return "Failed to upload file: " + e.getMessage();
        }
    }
}

