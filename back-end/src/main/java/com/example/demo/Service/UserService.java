package com.example.demo.Service;

import com.example.demo.DAO.Response.UserProfileResponse;
import com.example.demo.DAO.Response.UsersPointandEventResponse;
import com.example.demo.Entity.Event;
import com.example.demo.Entity.User;
import com.example.demo.Repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    @Autowired
    private final UserRepo userRepo;
    @Autowired
    private final AttendanceRepo attendanceRepo;
    @Autowired
    private final PointRepo pointRepo;
    @Autowired
    private final BadgeRepo badgeRepo;
    @Autowired
    private final RegisterRepo registerRepo;
    @Autowired
    private final EventRepo eventRepo;

    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                return userRepo.findByEmail(username)
                        .orElseThrow(() -> new UsernameNotFoundException("User not found"));
            }
        };
    }

    public User markUserActivated(Long id){
        User user = userRepo.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("User Not Found"));

        user.setActivated(true);

        return userRepo.save(user);
    }

    public User updateProfilePic(Path newPath, Long userId) {
        Optional<User> userOptional = userRepo.findById(userId);

        if (userOptional.isEmpty()) {
            throw new UsernameNotFoundException("User not found");
        }

        User user = userOptional.get();
        String currentProfilePath = user.getProfile();

        // Remove the current profile picture file
        if (currentProfilePath != null && !currentProfilePath.isEmpty()) {
            try {
                Files.deleteIfExists(Paths.get(currentProfilePath));
                user.setProfile(String.valueOf(newPath));
            } catch (IOException e) {
                // Handle the exception as needed
                e.printStackTrace();
            }
        }

        // Set the new profile picture path
        user.setProfile(String.valueOf(newPath));
        return userRepo.save(user);
    }

    public boolean isEmailAlreadyRegistered(String email) {
        return userRepo.existsByEmail(email);
    }

    public List<UsersPointandEventResponse> getAllUsersWithPointsAndEvents() {
        UsersPointandEventResponse response = new UsersPointandEventResponse();
        List<User> users = userRepo.findAll();
        List<UsersPointandEventResponse> usersPointandEventResponseList = new ArrayList<>();
        for (User user : users) {
            UsersPointandEventResponse usersPointandEventResponse = new UsersPointandEventResponse();
            usersPointandEventResponse.setId(user.getId());
            usersPointandEventResponse.setProfile(user.getProfile());
            usersPointandEventResponse.setEmail(user.getEmail());
            usersPointandEventResponse.setPoints(pointRepo.findByUserId(user.getId()));
            usersPointandEventResponse.setAttendances(attendanceRepo.findByUserId(user.getId()));

            usersPointandEventResponseList.add(usersPointandEventResponse);
        }

        return usersPointandEventResponseList;
    }

    public UserProfileResponse getUserProfile(Long id) {
        UserProfileResponse response = new UserProfileResponse();
        Optional<User> userOptional = userRepo.findById(id);
        if (userOptional.isEmpty()) {
            return new UserProfileResponse();
        }

        Integer points = pointRepo.calculateTotalPointsByUserId(id);

        if (points == null){
            points = 0;
        }
        User user = userOptional.get();
        response.setId(id);
        response.setFullname(user.getFullname());
        response.setEmail(user.getEmail());
        response.setPhone(user.getPhone());
        response.setGender(user.getGender());
        response.setProfile(user.getProfile());
        response.setPassword(user.getPassword());
        response.setPoint(points);
        response.setBadgeList(badgeRepo.findByPointsToEarnLessThan(points));
        response.setRegisterList(registerRepo.findTop5ByUseridOrderByRegisterdateDesc(id));
        List<Long> eventIds = response.getRegisterList();
        List<Event> eventList = new ArrayList<>();
        for (int i = 0; i < Math.min(5, eventIds.size()); i++) {
            Long eventId = eventIds.get(i);
            Event event = eventRepo.findByEventid(eventId);
            if (event != null) {
                eventList.add(event);
            } else {
                eventList.add(null);
            }
        }
        response.setEventList(eventList);

        return response;
    }
}