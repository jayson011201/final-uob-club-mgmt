package com.example.demo.Service;

import com.example.demo.Entity.PasswordResetToken;
import com.example.demo.Entity.User;
import com.example.demo.Repository.PasswordResetTokenRepo;
import com.example.demo.Repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class PasswordResetTokenService {
    @Autowired
    private PasswordResetTokenRepo passwordResetTokenRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public String generateToken() {
        return UUID.randomUUID().toString();
    }

    public PasswordResetToken createToken(User user, String token){
        PasswordResetToken resetToken = new PasswordResetToken();
        resetToken.setToken(token);
        resetToken.setUserId(user.getId());
        resetToken.setExpiryDate(LocalDateTime.now().plusHours(1));
        return passwordResetTokenRepo.save(resetToken);
    }

    public boolean verifyLink(Long userId, String token) {
        Optional<PasswordResetToken> tokenOptional = passwordResetTokenRepo.findByToken(token);

        if (tokenOptional.isPresent()) {
            PasswordResetToken passwordResetToken = tokenOptional.get();

            LocalDateTime expirationDate = passwordResetToken.getExpiryDate();
            return expirationDate.isAfter(LocalDateTime.now());
        } else {
            return false;
        }
    }

    public boolean resetPassword(String token, String newPassword) {
        Optional<PasswordResetToken> tokenOptional = passwordResetTokenRepo.findByToken(token);

        if (tokenOptional.isPresent()) {
            PasswordResetToken passwordResetToken = tokenOptional.get();

            if (verifyLink(passwordResetToken.getUserId(), passwordResetToken.getToken())) {
                Optional<User> userOptional = userRepo.findById(passwordResetToken.getUserId());

                if (userOptional.isPresent()) {
                    User user = userOptional.get();
                    user.setPassword(passwordEncoder.encode(newPassword));
                    userRepo.save(user);

                    // Delete the used token
                    deleteToken(passwordResetToken);

                    return true;
                }
            }
        }

        return false;
    }

    public Optional<PasswordResetToken> findByToken(String token) {
        return passwordResetTokenRepo.findByToken(token);
    }

    public void deleteToken(PasswordResetToken token) {
        passwordResetTokenRepo.delete(token);
    }
}
