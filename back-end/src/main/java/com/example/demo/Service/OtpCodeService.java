package com.example.demo.Service;

import com.example.demo.Entity.OtpCode;
import com.example.demo.Repository.OtpCodeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Random;

@Service
public class OtpCodeService {
    @Autowired
    private OtpCodeRepo otpCodeRepo;

    public String generateOtpCode(Long userId){
        String otp = String.format("%06d", new Random().nextInt(1000000));

        LocalDateTime expirationTime = LocalDateTime.now().plusMinutes(1);

        OtpCode otpCode = new OtpCode();
        otpCode.setUserId(userId);
        otpCode.setOtp(otp);
        otpCode.setExpirationTime(expirationTime);
        otpCodeRepo.save(otpCode);

        return otp;
    }

    public boolean verifyOtp(Long userId, String userOtp) {
        Optional<OtpCode> otpCodeOptional = otpCodeRepo.findByOtp(userOtp);

        if (otpCodeOptional.isPresent()) {
            OtpCode otpCode = otpCodeOptional.get();

            LocalDateTime expirationTime = otpCode.getExpirationTime();
            return expirationTime.isAfter(LocalDateTime.now());
        } else {
            return false;
        }
    }

}
