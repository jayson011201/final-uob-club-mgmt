package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    public void sendVerificationCode(String toEmail, String otpCode) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom("noreply@example.com");
        message.setTo(toEmail);
        message.setSubject("Your Verification Code");
        message.setText("Your verification code is: " + otpCode);

        mailSender.send(message);
    }
    public void sendResetPasswordLink(String toEmail, String token) {
        SimpleMailMessage message = new SimpleMailMessage();

        message.setFrom("noreply@example.com");
        message.setTo(toEmail);
        message.setSubject("Your Reset Password Link");
        message.setText("Your reset password link is: " + token);

        mailSender.send(message);
    }


    public String generateVerificationCode() {
        Random random = new Random();
        int code = random.nextInt(899999) + 100000; // This will always generate 6 digit number
        return String.valueOf(code);
    }
}
