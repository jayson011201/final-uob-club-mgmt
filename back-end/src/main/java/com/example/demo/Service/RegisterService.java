package com.example.demo.Service;

import com.example.demo.Entity.Register;
import com.example.demo.Repository.RegisterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class RegisterService {

    @Autowired
    private RegisterRepo registerRepo;

    public boolean hasUserRegisteredForEvent(Long userid, Long eventid) {
        return registerRepo.existsByUserIdAndEventId(userid, eventid);
    }

    public String registerUserForEvent(Long userid, Long eventid) {
        boolean isRegistered = hasUserRegisteredForEvent(userid, eventid);

        if (isRegistered) {
            return "User with ID " + userid + " is already registered for Event ID " + eventid;
        } else {
            Register newRegistration = new Register();
            newRegistration.setUserid(userid);
            newRegistration.setEventid(eventid);
            newRegistration.setRegisterdate(LocalDate.now());
            // Set other properties and handle registration date as needed

            registerRepo.save(newRegistration);

            return "User with ID " + userid + " has successfully registered for Event ID " + eventid;
        }
    }
}




