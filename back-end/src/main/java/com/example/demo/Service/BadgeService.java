package com.example.demo.Service;

import com.example.demo.Entity.Badge;
import com.example.demo.Repository.BadgeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BadgeService {
    private final BadgeRepo badgeRepo;

    @Autowired
    public BadgeService(BadgeRepo badgeRepo) {
        this.badgeRepo = badgeRepo;
    }

    public List<Badge> getAllBadges() {
        return badgeRepo.findAll();
    }

    public Optional<Badge> getBadgeById(Long id) {
        return badgeRepo.findById(id);
    }

    public Badge createBadge(Badge badge) {
        return badgeRepo.save(badge);
    }

    public Badge updateBadge(Long id, Badge updatedBadge) {
        return badgeRepo.findById(id)
                .map(badge -> {
                    badge.setName(updatedBadge.getName());
                    badge.setPointsToEarn(updatedBadge.getPointsToEarn());
                    return badgeRepo.save(badge);
                })
                .orElse(null); // Handle not found case
    }

    public void deleteBadge(Long id) {
        badgeRepo.deleteById(id);
    }
}
