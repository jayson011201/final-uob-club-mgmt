package com.example.demo.Service;

import com.example.demo.Entity.Point;
import com.example.demo.Repository.PointRepo;
import com.example.demo.Repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PointService {
    @Autowired
    private PointRepo pointRepo;

    @Autowired
    private UserRepo userRepo;

    public Point savePoint(Point newPoint) {
        Point point = new Point();
        point.setPointAmount(newPoint.getPointAmount());
        point.setPointEarnFrom(newPoint.getPointEarnFrom());
        point.setPointEarnDate(newPoint.getPointEarnDate());
        point.setUserId(newPoint.getUserId());

        return pointRepo.savePoint(point);
    }
}
