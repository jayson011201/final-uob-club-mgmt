package com.example.demo.Service;

import com.example.demo.DAO.Request.GoogleSignUpRequest;
import com.example.demo.DAO.Request.SignInRequest;
import com.example.demo.Entity.Role;
import com.example.demo.Repository.OtpCodeRepo;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.DAO.Request.SignUpRequest;
import com.example.demo.DAO.Response.JwtAuthenticationResponse;
import com.example.demo.Entity.User;
import com.example.demo.Repository.UserRepo;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AuthenticationService{
    private final UserRepo userRepo;
    private final OtpCodeRepo otpCodeRepo;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final EmailService emailService;
    private final OtpCodeService otpCodeService;

    public JwtAuthenticationResponse signup(SignUpRequest request) {
        var user = User.builder()
                .fullname(request.getFullname())
                .email(request.getEmail())
                .phone(request.getPhone())
                .gender(request.getGender())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.valueOf("USER"))
                .isActivated(false)
                .build();
        userRepo.save(user);
        //otpCodeService.generateOtpCode(user.getId());
        emailService.sendVerificationCode(user.getEmail(), otpCodeService.generateOtpCode(user.getId()));

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }

    public JwtAuthenticationResponse signin(SignInRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));

        var user = userRepo.findByEmail(request.getEmail())
                .orElseThrow(() -> new IllegalArgumentException("Invalid email or password."));

        if (user.isActivated() == false){
            throw new IllegalArgumentException("Account not activated. Please activate your account");
        }

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).user(user).build();
    }

    public JwtAuthenticationResponse googleSignUp(GoogleSignUpRequest request) {
        var user = User.builder()
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .role(Role.valueOf("USER"))
                .isActivated(false)
                .build();
        userRepo.save(user);
        emailService.sendVerificationCode(user.getEmail(), otpCodeService.generateOtpCode(user.getId()));

        var jwt = jwtService.generateToken(user);
        return JwtAuthenticationResponse.builder().token(jwt).build();
    }
}