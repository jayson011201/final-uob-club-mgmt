package com.example.demo.Controller;

import com.example.demo.Entity.Feedback;
import com.example.demo.Entity.Point;
import com.example.demo.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FeedbackController {

    @Autowired
    FeedbackRepo feedbackRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PointRepo pointRepo;

    @GetMapping("/view-feedback/{eventId}")
    public List<NameEmailStatus> findFeed(@PathVariable Long eventId){
        return feedbackRepo.findByEventId(eventId);
    }

    @GetMapping("/feedback-summary/{eventId}")
    public List<Summary> findSummary(@PathVariable Long eventId){
        return feedbackRepo.findSummary(eventId);
    }

    @GetMapping("/feedback-status/{userId}/{eventId}")
    public List<Status> findStatus(@PathVariable Long eventId, @PathVariable Long userId){
        return feedbackRepo.findUserStatus(eventId, userId);
    }

    @PutMapping("/feedback-form")
    public ResponseEntity<String> updateFeedback(@RequestBody Feedback newFeed){
        Optional<Feedback> opt = feedbackRepo.findByUserIdAndEventId(newFeed.getUserId(), newFeed.getEventId());

        LocalDate localDate = LocalDate.now();
        Date currentDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (opt.isPresent()){
            Feedback feed =opt.get();

            if (feed.getStatus().equals("No")){
                feed.setUserId(newFeed.getUserId());
                feed.setEventId(newFeed.getEventId());
                feed.setQuestion1(newFeed.getQuestion1());
                feed.setQuestion2(newFeed.getQuestion2());
                feed.setQuestion3(newFeed.getQuestion3());
                feed.setQuestion4(newFeed.getQuestion4());
                feed.setAnswer1(newFeed.getAnswer1());
                feed.setAnswer2(newFeed.getAnswer2());
                feed.setAnswer3(newFeed.getAnswer3());
                feed.setAnswer4(newFeed.getAnswer4());
                feed.setStatus("Yes");
                feed.setName(newFeed.getName());
                feed.setEmail(newFeed.getEmail());

                feedbackRepo.save(feed);

                if(newFeed.getStatus().equals("Yes")){
                    Point point = new Point();
                    point.setPointAmount(String.valueOf(5));
                    point.setPointEarnFrom("Feedback");
                    point.setPointEarnDate(LocalDate.now().toString());
                    point.setUserId(feed.getUserId().toString());

                    pointRepo.savePoint(point);
                }
            }

            return ResponseEntity.ok("Feedback record updated successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
