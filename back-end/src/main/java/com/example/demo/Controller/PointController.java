package com.example.demo.Controller;

import com.example.demo.DAO.Response.PointResponse;
import com.example.demo.Repository.PointRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "http://localhost:3000")
public class PointController {
    @Autowired
    private PointRepo pointRepo;

    @GetMapping("/points")
    public List<PointResponse> getAllPoints(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int pageSize
    ) {
        return pointRepo.getAllPointsWithUserEmail(page, pageSize);
    }
}
