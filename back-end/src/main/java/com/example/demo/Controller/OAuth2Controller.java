package com.example.demo.Controller;

import com.example.demo.DAO.Request.GoogleSignUpRequest;
import com.example.demo.DAO.Request.SignUpRequest;
import com.example.demo.DAO.Response.JwtAuthenticationResponse;
import com.example.demo.Entity.User;
import com.example.demo.Repository.UserRepo;
import com.example.demo.Service.AuthenticationService;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "http://localhost:3000")
public class OAuth2Controller {
    private final OAuth2AuthorizedClientService clientService;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    public OAuth2Controller(OAuth2AuthorizedClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping("/google-sign-up")
    public ResponseEntity<?> googleSignUp(@RequestBody GoogleSignUpRequest request) {
        if (userService.isEmailAlreadyRegistered(request.getEmail())) {
            return new ResponseEntity<>("Email already registered", HttpStatus.BAD_REQUEST);
        }
        JwtAuthenticationResponse response = authenticationService.googleSignUp(request);

        if (response == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/login/oauth2/code/facebook")
    public RedirectView fbLoginSuccess(@PathVariable String provider, OAuth2AuthenticationToken authenticationToken) {
        OAuth2AuthorizedClient client = clientService.loadAuthorizedClient(
                authenticationToken.getAuthorizedClientRegistrationId(),
                authenticationToken.getName()
        );

        String userEmail = (String) client.getPrincipalName();
        String provider1 = authenticationToken.getAuthorizedClientRegistrationId();

        Optional<User> existingUser = userRepo.findByEmail(userEmail);
        if (!existingUser.isPresent()) {
            User user = new User();
            user.setEmail(userEmail);
            user.setProvider(provider);
            userRepo.save(user);
        }

        return new RedirectView("/login-success");
    }
}
