package com.example.demo.Controller;

import com.example.demo.DAO.Request.PasswordResetTokenRequest;
import com.example.demo.Entity.PasswordResetToken;
import com.example.demo.Entity.User;
import com.example.demo.Repository.PasswordResetTokenRepo;
import com.example.demo.Repository.UserRepo;
import com.example.demo.Service.EmailService;
import com.example.demo.Service.PasswordResetTokenService;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "http://localhost:3000")
public class PasswordResetTokenController {

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordResetTokenRepo passwordResetTokenRepo;

    @Autowired
    private PasswordResetTokenService passwordResetTokenService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private PasswordResetTokenService tokenService;

    @PostMapping("/reset-password")
    public ResponseEntity<String> requestReset(@RequestBody PasswordResetTokenRequest request) {
        String email = request.getEmail();
        Optional<User> userOptional = userRepo.findByEmail(email);

        if (userOptional.isPresent()) {
            User user = userOptional.get();
            String token = tokenService.generateToken();
            PasswordResetToken resetToken = tokenService.createToken(user, token);

            String resetPasswordLink = "http://localhost:3000/reset-password?token=" + token;

            emailService.sendResetPasswordLink(email, resetPasswordLink);

            return ResponseEntity.ok("Reset link sent successfully");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
    }



    @GetMapping("/verify-link")
    public ResponseEntity<?> verifyLink(@RequestParam String token) {
        try {
            Optional<PasswordResetToken> tokenOptional = passwordResetTokenRepo.findByToken(token);
            if (tokenOptional.isEmpty()) {
                return ResponseEntity.badRequest().body("Invalid password reset token");
            }

            PasswordResetToken passwordResetToken = tokenOptional.get();
            Optional<User> userOptional = userRepo.findById(passwordResetToken.getUserId());

            if (userOptional.isEmpty()) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred during activation");
            }

            User user = userOptional.get();

            if (passwordResetTokenService.verifyLink(user.getId(), passwordResetToken.getToken())) {
                //userService.markUserActivated(user.getId());
                return ResponseEntity.ok("Link Authenticated");
            } else {
                return ResponseEntity.badRequest().body("Invalid password reset link");
            }

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred reset password");
        }
    }

    @PostMapping("/submit-reset-password")
    public ResponseEntity<String> submitResetPassword(@RequestBody Map<String, String> request) {
        String token = request.get("token");
        String newPassword = request.get("newPassword");

        boolean passwordResetSuccess = passwordResetTokenService.resetPassword(token, newPassword);

        if (passwordResetSuccess) {
            return ResponseEntity.ok("Password reset successfully");
        } else {
            return ResponseEntity.badRequest().body("Failed to reset password. Invalid or expired token.");
        }
    }


}
