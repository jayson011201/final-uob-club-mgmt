package com.example.demo.Controller;


import com.example.demo.Entity.Badge;
import com.example.demo.Repository.BadgeRepo;
import com.example.demo.Service.BadgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "http://localhost:3000")
public class BadgeController {
    private final BadgeService badgeService;

    @Autowired
    private BadgeRepo badgeRepo;

    @Value("${badge.file.path}")
    private String imagePath;

    @Autowired
    public BadgeController(BadgeService badgeService) {
        this.badgeService = badgeService;
    }

    @GetMapping("/badges")
    public List<Badge> getAllBadges() {
        return badgeService.getAllBadges();
    }

    @GetMapping("/get-badge/{id}")
    public ResponseEntity<Badge> getBadgeById(@PathVariable Long id) {
        Optional<Badge> badge = badgeService.getBadgeById(id);
        return badge.map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/new-badge")
    public Badge createBadge(@RequestParam("badgeImage") MultipartFile file, @RequestParam("name") String name, @RequestParam("pointsToEarn") String pointsToEarn) {
        Badge badge = new Badge();
        try {
            String uploadDirectory = imagePath;

            String originalFilename = file.getOriginalFilename();

            byte[] bytes = file.getBytes();
            Path filePath = Paths.get(uploadDirectory, originalFilename);
            Files.write(filePath, bytes);

            badge.setName(name);
            badge.setImage(String.valueOf(filePath));
            badge.setPointsToEarn(Integer.parseInt(pointsToEarn));
        } catch (Exception e) {
            return new Badge();
        }
        return badgeRepo.save(badge);
    }

    @PatchMapping("/edit-badge/{id}")
    public ResponseEntity<Badge> updateBadge(@PathVariable Long id, @RequestBody Badge updatedBadge) {
        Badge badge = badgeService.updateBadge(id, updatedBadge);
        return badge != null ? ResponseEntity.ok(badge) : ResponseEntity.notFound().build();
    }

    @DeleteMapping("/delete-badge/{id}")
    public ResponseEntity<Void> deleteBadge(@PathVariable Long id) {
        badgeService.deleteBadge(id);
        return ResponseEntity.noContent().build();
    }
}
