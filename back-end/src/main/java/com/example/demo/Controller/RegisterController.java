package com.example.demo.Controller;

import com.example.demo.Entity.Attendance;
import com.example.demo.Entity.Feedback;
import com.example.demo.Entity.Register;
import com.example.demo.Entity.User;
import com.example.demo.Repository.AttendanceRepo;
import com.example.demo.Repository.FeedbackRepo;
import com.example.demo.Repository.RegisterRepo;
import com.example.demo.Repository.UserRepo;
import com.example.demo.Service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RegisterController {

    @Autowired
    RegisterRepo registerRepo;
    @Autowired
    AttendanceRepo attendanceRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    FeedbackRepo feedbackRepo;
    @Autowired
    RegisterService registerService;

    @GetMapping("/register")
    public Iterable<Register> findAllRegister(){
        return registerRepo.findAll();
    }

    @GetMapping("/checkRegistration/{userid}/{eventid}")
    public boolean checkRegistration(@PathVariable Long userid, @PathVariable Long eventid) {
        boolean isRegistered = registerService.hasUserRegisteredForEvent(userid, eventid);
        return registerService.hasUserRegisteredForEvent(userid, eventid);
    }

    @PostMapping("/register/{userid}/{eventid}")
    public String registerForEvent(@PathVariable Long userid, @PathVariable Long eventid) {
        Optional<User> optionalUser = userRepo.findById(userid);
        if (optionalUser.isEmpty()){
            return "user not found";
        }

        User user = optionalUser.get();

        Attendance att = new Attendance();
        att.setEventId(eventid);
        att.setUserId(userid);
        att.setStatus("No");
        att.setName(user.getFullname());
        att.setEmail(user.getEmail());
        attendanceRepo.save(att);

        Feedback feed = new Feedback();
        feed.setEventId(eventid);
        feed.setUserId(userid);
        feed.setStatus("No");
        feed.setName(user.getFullname());
        feed.setEmail(user.getEmail());
        feedbackRepo.save(feed);

        return registerService.registerUserForEvent(userid, eventid);
    }
}





