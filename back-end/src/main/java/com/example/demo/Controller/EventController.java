package com.example.demo.Controller;

import DTO.EventCategoryResp;
import DTO.Resp;
import com.example.demo.Entity.Event;
import com.example.demo.Repository.EventIdName;
import com.example.demo.Repository.EventRepo;
import com.example.demo.Service.EventService;
import com.example.demo.Service.ImageStorageService;
import com.example.demo.Service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin(origins = "http://localhost:3000")

public class EventController {

    @Autowired
    EventRepo eventRepo;
    @Autowired
    RegisterService registerService;

    @Autowired
    EventService eventService;

    @Autowired
    private ImageStorageService imageStorageService;

    @GetMapping("/events") //list all events
    public Resp findAllEvents() {
        Iterable<Event> event = eventRepo.findAll();
        return new Resp(200, event);
    }

    @GetMapping("/events/{id}") // Fetch event details by ID
    public Resp findEventById(@PathVariable Long id) {
        Optional<Event> event = eventRepo.findById(id);
        return event.map(value -> new Resp(200, value))
                .orElseGet(() -> new Resp(404, "Event not found"));
    }

    @GetMapping("/event-attendance")
    public List<EventIdName> findEvent(){
        return eventRepo.findEvent();
    }




//    @GetMapping("/events-categories") // list events based on date categories
//    public Resp listEventsByDateCategory() {
//        LocalDate currentDate = LocalDate.now(); // Get the current date
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//
//        Iterable<Event> events = eventRepo.findAll(); // Retrieve all events
//        List<Event> currentEvents = new ArrayList<>(); // Retrieve current events (live)
//        List<Event> upcomingEvents = new ArrayList<>(); // Retrieve upcoming events
//        List<Event> pastEvents = new ArrayList<>(); // Retrieve past events
//
//        for (Event event : events) {
//            LocalDate eventDate = event.getEventdate(); // Assuming your event date is in "yyyy-MM-dd" format
//
//            if (eventDate.isEqual(currentDate)) {
//                currentEvents.add(event);
//            } else if (eventDate.isAfter(currentDate)) {
//                upcomingEvents.add(event);
//            } else {
//                pastEvents.add(event);
//            }
//        }
//
//        EventCategoryResp response = new EventCategoryResp();
//        response.setCurrentEvents(currentEvents);
//        response.setUpcomingEvents(upcomingEvents);
//        response.setPastEvents(pastEvents);
//
//        return new Resp(200, response);
//    }

    @GetMapping("/events-categories/{userId}")
    public Resp listEventsByDateCategory(@PathVariable Long userId) {
        LocalDate currentDate = LocalDate.now(); // Get the current date
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Iterable<Event> events = eventRepo.findAll(); // Retrieve all events
        List<Event> currentEvents = new ArrayList<>(); // Retrieve current events (live)
        List<Event> upcomingEvents = new ArrayList<>(); // Retrieve upcoming events
        List<Event> pastEvents = new ArrayList<>(); // Retrieve past events
        List<Event> registeredEvents = new ArrayList<>(); // Retrieve registered events for the user

        for (Event event : events) {
            LocalDate eventDate = event.getEventdate(); // Assuming your event date is in "yyyy-MM-dd" format

            if (eventDate.isEqual(currentDate)) {
                currentEvents.add(event);
            } else if (eventDate.isAfter(currentDate)) {
                upcomingEvents.add(event);
            } else {
                pastEvents.add(event);
            }

            // Assuming you have a RegisterService to check registration and joining status
            boolean isRegistered = registerService.hasUserRegisteredForEvent(userId, event.getEventid());
//            boolean hasJoined = registerService.hasUserJoinedEvent(userId, event.getEventid());

            if (isRegistered) {
                registeredEvents.add(event);
            }

        }

        EventCategoryResp response = new EventCategoryResp();
        response.setCurrentEvents(currentEvents);
        response.setUpcomingEvents(upcomingEvents);
        response.setPastEvents(pastEvents);
        response.setRegisteredEvents(registeredEvents);

        return new Resp(200, response);
    }


    @PostMapping("/create-event")
    public Event createEvent(@ModelAttribute Event event, @RequestParam("file") MultipartFile file) {
        try {
            String imagePath = imageStorageService.storeImage(file);
            event.setEventimages(imagePath);

            return eventRepo.save(event);
        } catch (IOException e) {
            e.printStackTrace();
            return new Event();
        }
    }

    @PutMapping("/update-link/{eventId}")
    public ResponseEntity<?> updateEventAttendanceLink(@PathVariable Long eventId, @RequestBody Event updatedEvent) {
        try {
            Optional<Event> existingEvent = eventRepo.findById(eventId);

            if (existingEvent.isPresent()) {
                // Update the attendance link
                Event event = existingEvent.get();
                event.setAttendanceLink(updatedEvent.getAttendanceLink());

                // Save the updated event to the database
                eventRepo.save(event);

                return ResponseEntity.ok("Attendance link updated successfully.");
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to update attendance link.");
        }
    }



    @PatchMapping("/update-event/{id}") //update event
    public Resp update(@RequestBody Event partialEvent, @PathVariable Long id) {
        Optional<Event> opt = eventRepo.findById(id);
        if (opt.isPresent()) {
            Event event = opt.get();

            // Check each field in the partialEvent and update the corresponding field in the original event
            if (partialEvent.getEventname() != null) {
                event.setEventname(partialEvent.getEventname());
            }
            if (partialEvent.getEventdesc() != null) {
                event.setEventdesc(partialEvent.getEventdesc());
            }
            if (partialEvent.getEventtime() != null) {
                event.setEventtime(partialEvent.getEventtime());
            }
            if (partialEvent.getEventdate() != null) {
                event.setEventdate(partialEvent.getEventdate());
            }
            if (partialEvent.getEventvenue() != null) {
                event.setEventvenue(partialEvent.getEventvenue());
            }
            if (partialEvent.getEventcapacity() != null) {
                event.setEventvenue(partialEvent.getEventvenue());
            }
            if (partialEvent.getEventimages() != null) {
                event.setEventimages(partialEvent.getEventimages());
            }
            if (partialEvent.getRegisterbefore() != null) {
                event.setRegisterbefore(partialEvent.getRegisterbefore());
            }
            if (partialEvent.getAdminid() != null) {
                event.setAdminid(partialEvent.getAdminid());
            }
            eventRepo.save(event);
            return new Resp(200, event);
        }

        return new Resp(404, "ID does not exist");
    }

    @DeleteMapping("/delete-event/{id}") // delete event by ID
    public Resp deleteEventById(@PathVariable Long id) {
        Optional<Event> eventOptional = eventRepo.findById(id);

        if (eventOptional.isPresent()) {
            Event event = eventOptional.get();
            eventRepo.delete(event);
            return new Resp(200, "Event deleted successfully");
        }

        return new Resp(404, "Event not found");
    }

    @GetMapping("/images/evimages/{imageName}")
    public ResponseEntity<Resource> getBadgeImage(@PathVariable String imageName) throws MalformedURLException {
        Path imagePath = Paths.get("src/main/resources/static/images/evimages").resolve(imageName);
        Resource resource = new UrlResource(imagePath.toUri());

        return ResponseEntity.ok().body(resource);
    }

}
