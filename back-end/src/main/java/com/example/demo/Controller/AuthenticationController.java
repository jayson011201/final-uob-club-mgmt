package com.example.demo.Controller;

import com.example.demo.DAO.Request.ActivationCodeRequest;
import com.example.demo.DAO.Request.PasswordResetTokenRequest;
import com.example.demo.DAO.Request.SignInRequest;
import com.example.demo.Entity.OtpCode;
import com.example.demo.Entity.User;
import com.example.demo.Repository.OtpCodeRepo;
import com.example.demo.Repository.UserRepo;
import com.example.demo.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import com.example.demo.DAO.Request.SignUpRequest;
import com.example.demo.DAO.Response.JwtAuthenticationResponse;

import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class AuthenticationController {
    private final AuthenticationService authenticationService;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private OtpCodeService otpCodeService;

    @Autowired
    private OtpCodeRepo otpCodeRepo;

    @PostMapping("/signup")
    public ResponseEntity<?> signup(@RequestBody SignUpRequest request) {
        if (userService.isEmailAlreadyRegistered(request.getEmail())) {
            return new ResponseEntity<>("Email already registered", HttpStatus.BAD_REQUEST);
        }
        JwtAuthenticationResponse response = authenticationService.signup(request);

        if (response == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(response);
    }

    @PostMapping("/resend-otp")
    public ResponseEntity<?> resendOtp(@RequestBody PasswordResetTokenRequest request){
        Optional<User> userOptional = userRepo.findByEmail(request.getEmail());
        if (userOptional.isPresent()){
            User user = userOptional.get();
            emailService.sendVerificationCode(request.getEmail(), otpCodeService.generateOtpCode(user.getId()));
            return ResponseEntity.ok("Otp Code resend");
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not found");
    }

    @PostMapping("/verify-otp")
    public ResponseEntity<?> verify(@RequestBody ActivationCodeRequest activationCodeRequest) {
        try {
            Optional<OtpCode> otpCodeOptional = otpCodeRepo.findByOtp(activationCodeRequest.getCode());

            if (otpCodeOptional.isEmpty()) {
                return ResponseEntity.badRequest().body("Invalid activation code");
            }

            OtpCode otpCode = otpCodeOptional.get();
            Optional<User> userOptional = userRepo.findById(otpCode.getUserId());

            if (userOptional.isEmpty()) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred during activation");
            }

            User user = userOptional.get();

            if (otpCodeService.verifyOtp(user.getId(), otpCode.getOtp())) {
                userService.markUserActivated(user.getId());
                return ResponseEntity.ok("Activation Successfully");
            } else {
                return ResponseEntity.badRequest().body("Invalid activation code");
            }

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred during activation");
        }
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> signin(@RequestBody SignInRequest request) {
        return ResponseEntity.ok(authenticationService.signin(request));
    }

//    @PostMapping("/forgot-password")
//    public ResponseEntity<?> sendVerificationCode(@RequestBody PasswordResetTokenRequest request){
//        try{
//            emailService.sendVerificationCode(request.getEmail());
//            return ResponseEntity.ok("Verification code sent successfully");
//        }catch (Exception e){
//            return ResponseEntity.badRequest().body("Failed to send verification code");
//        }
//    }

    @GetMapping("/home")
    public String Home(){
        return "Hello World. Home Page";
    }

    @GetMapping("/home2")
    public String Home2(){
        return "Hello World. Home Page 2";
    }

    @GetMapping("/secured")
    @PreAuthorize("isAuthenticated()")
    public String Secured(){
        return "This is a secured page";
    }
}