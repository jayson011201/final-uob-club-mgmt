package com.example.demo.Controller;

import com.example.demo.DAO.Request.UpdateUserRequest;
import com.example.demo.DAO.Response.UserProfileResponse;
import com.example.demo.DAO.Response.UsersPointandEventResponse;
import com.example.demo.Entity.User;
import com.example.demo.Repository.UserRepo;
import com.example.demo.Service.UserService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

    @Value("${file.path}")
    private String imagePath;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return (List<User>) userRepo.findAll();
    }

    @GetMapping("/users/{id}")
    public Optional<User> getUserById(@PathVariable Long id) {
        return userRepo.findById(id);
    }

    @GetMapping("/email/{email}")
    public Optional<User> getUserByEmail(@PathVariable String email) {
        return userRepo.findByEmail(email);
    }

    @PostMapping
    public User createUser(@RequestBody User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return userRepo.save(user);
    }

    @PutMapping("/{id}")
    public User updateUser(@PathVariable Long id, @RequestBody User updatedUser) {
        if (userRepo.existsById(id)) {
            updatedUser.setId(id);
            return userRepo.save(updatedUser);
        }
        return null;
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<?> updateUserProfile(@PathVariable Long id, @RequestBody UpdateUserRequest request) {
        try {
            // Find the user by ID
            Optional<User> userOptional = userRepo.findById(id);

            if (userOptional == null) {
                return new ResponseEntity<>("User not found", HttpStatus.NOT_FOUND);
            }

            User user = userOptional.get();

            // Update user profile fields only if they are provided in the request
            if (request.getFullname() != null) {
                user.setFullname(request.getFullname());
            }

            if (request.getPhone() != null) {
                user.setPhone(request.getPhone());
            }

            if (request.getGender() != null) {
                user.setGender(request.getGender());
            }

            // Save the updated user
            userRepo.save(user);

            return new ResponseEntity<>("User profile updated successfully", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Error updating user profile", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/images/{imageName}")
    public ResponseEntity<Resource> getImage(@PathVariable String imageName) throws MalformedURLException {
        Path imagePath = Paths.get("src/main/resources/static/images").resolve(imageName);
        Resource resource = new UrlResource(imagePath.toUri());

        return ResponseEntity.ok().body(resource);
    }

    @GetMapping("/images/badges/{imageName}")
    public ResponseEntity<Resource> getBadgeImage(@PathVariable String imageName) throws MalformedURLException {
        Path imagePath = Paths.get("src/main/resources/static/images/badges").resolve(imageName);
        Resource resource = new UrlResource(imagePath.toUri());

        return ResponseEntity.ok().body(resource);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable Long id) {
        userRepo.deleteById(id);
    }

    @PostMapping("/upload-profile-pic")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, @RequestParam("id") Long userId) {
        try {
            String uploadDirectory = imagePath;

            String originalFilename = file.getOriginalFilename();

            byte[] bytes = file.getBytes();
            Path filePath = Paths.get(uploadDirectory, originalFilename);
            Files.write(filePath, bytes);

            userService.updateProfilePic(filePath, userId);

            return "File uploaded successfully! " + filePath;
        } catch (Exception e) {
            return "Failed to upload file: " + e.getMessage();
        }
    }

    @GetMapping("/users/get-profile-picture/{id}")
    public Optional<User> getProfilePictureById(@PathVariable Long id) {
        return userRepo.findById(id);
    }

    @GetMapping("/profile-images/{imageName}")
    public byte[] getProfileImage(@PathVariable String imageName) throws IOException {
        InputStream in = getClass()
                .getResourceAsStream("/images/" + imageName);
        return IOUtils.toByteArray(in);
    }

    @GetMapping("/users-points-attendance")
    public List<UsersPointandEventResponse> getAllUsersWithPointsAndEvents() {
        return userService.getAllUsersWithPointsAndEvents();
    }

    @GetMapping("/user-profile/{id}")
    public UserProfileResponse getUserProfileDetails(@PathVariable Long id) {
        return userService.getUserProfile(id);
    }

}