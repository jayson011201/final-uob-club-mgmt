package com.example.demo.Controller;

import com.example.demo.Entity.Attendance;
import com.example.demo.Entity.Point;
import com.example.demo.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class AttendanceController {

    @Autowired
    AttendanceRepo attendanceRepo;

    @Autowired
    PointRepo pointRepo;

    @Autowired
    UserRepo userRepo;

    @GetMapping("/view-attendance/{eventId}")
    public List<NameEmailStatus> findAtt(@PathVariable Long eventId) {
        return attendanceRepo.findByEventId(eventId);
    }

    @GetMapping("/attendance-summary/{eventId}")
    public List<Status> findSummary(@PathVariable Long eventId) {
        return attendanceRepo.findStatus(eventId);
    }

    @GetMapping("/attendance-status/{userId}/{eventId}")
    public List<Status> findStatus(@PathVariable Long eventId, @PathVariable Long userId) {
        return attendanceRepo.findUserStatus(eventId, userId);
    }

    @PutMapping("/attendance-form")
    public ResponseEntity<String> updateAttendance(@RequestBody Attendance newAtt) {
        Optional<Attendance> opt = attendanceRepo.findByUserIdAndEventId(newAtt.getUserId(), newAtt.getEventId());

        LocalDate localDate = LocalDate.now();
        Date currentDate = Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        if (opt.isPresent()) {
            Attendance att = opt.get();

            if (att.getStatus().equals("No")){
                att.setStatus("Yes");
                att.setName(newAtt.getName());
                att.setEmail(newAtt.getEmail());
                att.setAttendanceDate(currentDate);
                att.setUserId(newAtt.getUserId());
                att.setEventId(newAtt.getEventId());
                attendanceRepo.save(att);

                if (att.getStatus().equals("Yes")) {
                    Point point = new Point();
                    point.setPointAmount(String.valueOf(5));
                    point.setPointEarnFrom("Attendance");
                    point.setPointEarnDate(LocalDate.now().toString());
                    point.setUserId(newAtt.getUserId().toString());

                    pointRepo.savePoint(point);
                }
            }

            return ResponseEntity.ok("Attendance record is updated successfully");
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
