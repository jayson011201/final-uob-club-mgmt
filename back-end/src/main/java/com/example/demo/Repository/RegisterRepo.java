package com.example.demo.Repository;

import com.example.demo.Entity.Register;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegisterRepo extends CrudRepository <Register, Long> {
    List<Register> findByUserid(Long userid);
    @Query("SELECT COUNT(r) > 0 FROM Register r WHERE r.userid = :userid AND r.eventid = :eventid")
    boolean existsByUserIdAndEventId(Long userid, Long eventid);

    @Query("SELECT r.eventid FROM Register r WHERE r.userid = :userid ORDER BY r.registerdate DESC")
    List<Long> findTop5ByUseridOrderByRegisterdateDesc(@Param("userid") Long userid);

}




