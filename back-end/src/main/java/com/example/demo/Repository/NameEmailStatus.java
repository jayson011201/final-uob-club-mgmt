package com.example.demo.Repository;

public interface NameEmailStatus {
    String getName();

    String getEmail();

    String getStatus();
}
