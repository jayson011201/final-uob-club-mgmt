package com.example.demo.Repository;

import com.example.demo.Entity.Feedback;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface FeedbackRepo extends CrudRepository<Feedback, Long> {
    Optional<Feedback> findByUserIdAndEventId(Long userId, Long eventId);


    @Query("SELECT a FROM Feedback a WHERE a.eventId = ?1")
    List<NameEmailStatus> findByEventId(Long eventId);

    @Query("SELECT a FROM Feedback a WHERE a.eventId = ?1")
    List<Summary> findSummary(Long eventId);

    @Query("SELECT a FROM Feedback a WHERE a.eventId = ?1 and a.userId =?1")
    List<Status> findUserStatus(Long eventId, Long userId);


}

