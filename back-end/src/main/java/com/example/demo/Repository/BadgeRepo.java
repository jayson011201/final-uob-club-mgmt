package com.example.demo.Repository;

import com.example.demo.Entity.Badge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BadgeRepo extends JpaRepository<Badge, Long> {
    @Query("SELECT b FROM Badge b WHERE b.pointsToEarn < ?1")
    List<Badge> findByPointsToEarnLessThan(int totalPoints);
}
