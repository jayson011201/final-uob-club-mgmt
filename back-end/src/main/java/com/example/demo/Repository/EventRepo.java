package com.example.demo.Repository;

import com.example.demo.Entity.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepo extends CrudRepository<Event, Long> {
    @Query("SELECT e FROM Event e ORDER BY e.registerbefore DESC")
    List<Event> findLatestRegisteredEventsLimit5();

    @Query("SELECT a FROM Event a")
    List<EventIdName> findEvent();

    Event findByEventid(Long eventid);
}
