package com.example.demo.Repository;

import com.example.demo.Entity.Attendance;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AttendanceRepo extends CrudRepository<Attendance, Long> {

    Optional<Attendance> findByUserIdAndEventId(Long userId, Long eventId);

    @Query("SELECT a FROM Attendance a WHERE a.eventId = ?1")
    List<NameEmailStatus> findByEventId(Long eventId);

    @Query("SELECT a FROM Attendance a WHERE a.eventId = ?1")
    List<Status> findStatus(Long eventId);

    @Query("SELECT a FROM Attendance a WHERE a.eventId = ?1 and a.userId =?1")
    List<Status> findUserStatus(Long eventId, Long userId);

    @Query("SELECT a FROM Attendance a WHERE a.userId = ?1")
    List<Attendance> findByUserId(Long userId);





}
