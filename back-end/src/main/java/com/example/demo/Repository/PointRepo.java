package com.example.demo.Repository;

import com.example.demo.DAO.Response.PointResponse;
import com.example.demo.Entity.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PointRepo {

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public PointRepo(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public List<PointResponse> getAllPointsWithUserEmail(int page, int pageSize) {
        String sqlQuery = "SELECT point.id, point.point_amount, point.point_earn_from, " +
                "point.point_earn_date, user.email " +
                "FROM point " +
                "JOIN user ON point.user_id = user.id " +
                "LIMIT :pageSize OFFSET :offset";

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("pageSize", pageSize);
        parameters.addValue("offset", page * pageSize);

        return namedParameterJdbcTemplate.query(
                sqlQuery,
                parameters,
                (resultSet, rowNum) -> {
                    PointResponse pointResponse = new PointResponse();
                    pointResponse.setId(resultSet.getLong("id"));
                    pointResponse.setPointAmount(resultSet.getString("point_amount"));
                    pointResponse.setPointEarnFrom(resultSet.getString("point_earn_from"));
                    pointResponse.setPointEarnDate(resultSet.getString("point_earn_date"));
                    pointResponse.setEmail(resultSet.getString("email"));
                    return pointResponse;
                }
        );
    }

    public List<PointResponse> findByUserId(Long id) {
        String sqlQuery = "SELECT point.id, point.point_amount, point.point_earn_from, " +
                "point.point_earn_date, user.email " +
                "FROM point " +
                "JOIN user ON point.user_id = user.id " +
                "WHERE point.user_id = " + id;


        return namedParameterJdbcTemplate.query(
                sqlQuery,
                new MapSqlParameterSource(),
                (resultSet, rowNum) -> {
                    PointResponse pointResponse = new PointResponse();
                    pointResponse.setId(resultSet.getLong("id"));
                    pointResponse.setPointAmount(resultSet.getString("point_amount"));
                    pointResponse.setPointEarnFrom(resultSet.getString("point_earn_from"));
                    pointResponse.setPointEarnDate(resultSet.getString("point_earn_date"));
                    pointResponse.setEmail(resultSet.getString("email"));
                    return pointResponse;
                }
        );
    }

    public Integer calculateTotalPointsByUserId(Long id) {
        String sqlQuery = "SELECT SUM(point_amount) AS total_points " +
                "FROM point " +
                "WHERE user_id = :userId";

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", id);

        Integer totalPoints = namedParameterJdbcTemplate.queryForObject(
                sqlQuery,
                parameters,
                (resultSet, rowNum) -> {
                    Integer points = resultSet.getInt("total_points");
                    return resultSet.wasNull() ? null : points;
                }
        );

        return totalPoints != null ? totalPoints : 0; // Return 0 if totalPoints is null
    }

    public Point savePoint(Point point) {
        String sqlQuery = "INSERT INTO point (user_id, point_amount, point_earn_from, point_earn_date) " +
                "VALUES (:userId, :pointAmount, :pointEarnFrom, :pointEarnDate)";

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("userId", point.getUserId());
        parameters.addValue("pointAmount", point.getPointAmount());
        parameters.addValue("pointEarnFrom", point.getPointEarnFrom());
        parameters.addValue("pointEarnDate", point.getPointEarnDate());

        namedParameterJdbcTemplate.update(sqlQuery, parameters);
        return point;
    }

}
