package com.example.demo.Repository;

import com.example.demo.Entity.OtpCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OtpCodeRepo extends JpaRepository<OtpCode, Long> {
    Optional<OtpCode> findByOtp(String otp);
}
