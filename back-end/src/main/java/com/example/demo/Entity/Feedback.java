package com.example.demo.Entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Feedback {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long feedbackId;

    private  Long userId;
    private  Long eventId;
    private String question1;
    private String question2;
    private String question3;
    private String question4;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private  String status;
    private  String name;
    private  String email;
}
