package com.example.demo.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
//@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eventid")
    private Long eventid;

    @Column(name = "eventname")
    private String eventname;

    @Column(name = "eventdesc")
    private String eventdesc;

    @Column(name = "eventdate")
    private LocalDate eventdate;

    @Column(name = "eventtime")
    private LocalTime eventtime;

    @Column(name = "eventvenue")
    private String eventvenue;

    @Column(name = "eventcapacity")
    private Integer eventcapacity;

    @Column(name = "eventimages")
    private String eventimages;

    @Column(name = "registerbefore")
    private LocalDate registerbefore;

    @Column(name = "adminid")
    private String adminid;

    private String attendanceLink;

    // Getter and setter


    public Long getEventid() {
        return eventid;
    }

    public void setEventid(Long eventid) {
        this.eventid = eventid;
    }

    public String getEventname() {
        return eventname;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }

    public String getEventdesc() {
        return eventdesc;
    }

    public void setEventdesc(String eventdesc) {
        this.eventdesc = eventdesc;
    }

    public LocalTime getEventtime(){return eventtime;}

    public void setEventtime(LocalTime eventtime){this.eventtime = eventtime; }

    public LocalDate getEventdate() {
        return eventdate;
    }

    public void setEventdate(LocalDate eventdate) {
        this.eventdate = eventdate;
    }

    public String getEventvenue() {
        return eventvenue;
    }

    public void setEventvenue(String eventvenue) {
        this.eventvenue = eventvenue;
    }

    public Integer getEventcapacity() {
        return eventcapacity;
    }

    public void setEventcapacity(Integer eventcapacity) {
        this.eventcapacity = eventcapacity;
    }

    public String getEventimages() {
        return eventimages;
    }

    public void setEventimages(String eventimages) {
        this.eventimages = eventimages;
    }

    public LocalDate getRegisterbefore() {
        return registerbefore;
    }

    public void setRegisterbefore(LocalDate registerbefore) {
        this.registerbefore = registerbefore;
    }

    public String getAdminid() {
        return adminid;
    }

    public void setAdminid(String adminid) {
        this.adminid = adminid;
    }

    //Constructor

    public Event() {
    }

}
