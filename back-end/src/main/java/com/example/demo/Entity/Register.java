package com.example.demo.Entity;

import jakarta.persistence.*;

import java.lang.reflect.Constructor;
import java.time.LocalDate;

@Entity
public class Register {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "registerid")
    private Long registerid;

    @Column(name = "userid")
    private Long userid;

    @Column(name = "eventid")
    private Long eventid;

    @Column(name = "registerdate")
    private LocalDate registerdate;

    // Getter and setter


    public Long getRegisterid() {
        return registerid;
    }

    public void setRegisterid(Long registerid) {
        this.registerid = registerid;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public Long getEventid() {
        return eventid;
    }

    public void setEventid(Long eventid) {
        this.eventid = eventid;
    }

    public LocalDate getRegisterdate() {
        return registerdate;
    }

    public void setRegisterdate(LocalDate registerdate) {
        this.registerdate = registerdate;
    }

    // Constructor


    public Register() {
    }
}
