package com.example.demo.DAO.Response;

import com.example.demo.Entity.Badge;
import com.example.demo.Entity.Event;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserProfileResponse {
    private Long id;

    private String fullname;

    private String email;

    private String gender;

    private String phone;

    private String password;

    private String profile;

    private int point;

    private List<Badge> badgeList;

    private List<Long> registerList;

    private List<Event> eventList;
}
