package com.example.demo.DAO.Response;

import com.example.demo.Entity.Attendance;
import com.example.demo.Entity.Point;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UsersPointandEventResponse {
    private Long id;

    private String profile;

    private String email;

    private List<PointResponse> points;

    private List<Attendance> attendances;
}
