package com.example.demo.DAO.Request;

import com.example.demo.Entity.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SignUpRequest {
    private String fullname;

    private String email;

    private String gender;

    private String phone;

    private String password;

    private Role role;
}
