package com.example.demo.DAO.Request;

import com.example.demo.Entity.Role;
import lombok.*;

@Data
@Builder
@Getter
@Setter
public class UpdateUserRequest {
    private String fullname;

    private String gender;

    private String phone;
}
