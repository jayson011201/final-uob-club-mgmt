package com.example.demo.DAO.Request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EventImageRequest {
    private Long eventid;

    private String eventname;

    private String eventdesc;

    private LocalDate eventdate;

    private LocalTime eventtime;

    private String eventvenue;

    private Integer eventcapacity;

    private MultipartFile eventimages;

    private LocalDate registerbefore;

    private String adminid;
}
