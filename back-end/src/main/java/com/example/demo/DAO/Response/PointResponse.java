package com.example.demo.DAO.Response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointResponse {

    private Long id;

    private String pointAmount;

    private String pointEarnFrom;

    private String pointEarnDate;

    private String email;
}
