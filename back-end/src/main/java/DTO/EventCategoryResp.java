package DTO;

import com.example.demo.Entity.Event;

import java.util.List;

public class EventCategoryResp {
    private List<Event> currentEvents;
    private List<Event> upcomingEvents;
    private List<Event> pastEvents;

    private List<Event> registeredEvents;


    //Getter and setter


    public List<Event> getCurrentEvents() {
        return currentEvents;
    }

    public void setCurrentEvents(List<Event> currentEvents) {
        this.currentEvents = currentEvents;
    }

    public List<Event> getUpcomingEvents() {
        return upcomingEvents;
    }

    public void setUpcomingEvents(List<Event> upcomingEvents) {
        this.upcomingEvents = upcomingEvents;
    }

    public List<Event> getPastEvents() {
        return pastEvents;
    }

    public void setPastEvents(List<Event> pastEvents) {
        this.pastEvents = pastEvents;
    }

    public List<Event> getRegisteredEvents() {
        return registeredEvents;
    }

    public void setRegisteredEvents(List<Event> registeredEvents) {
        this.registeredEvents = registeredEvents;
    }


    //Constructor


    public EventCategoryResp() {
    }
}


